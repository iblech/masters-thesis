\chapter{\EilenbergMacLane{} spaces in HoTT}

In classical homotopy theory, it is well known that for any CW-complex~\(X\) and any abelian group~\(G\) and \(n \in \mathbb{N}\), the set \([X, K(G, n)]\) of homotopy classes of maps from \(X\) to the \emph{Eilenberg--MacLane space}~\(K(G, n)\) is in natural bijection to the singular cohomology group~\(H^n(X; G)\). The Eilenberg--MacLane~\(K(G, n)\) space is a space whose \(n\)th homotopy group is isomorphic to~\(G\) and whose other homotopy groups all vanish.

In homotopy type theory, we cannot use the definition of cohomology using the singular cochain complex since this construction is not homotopy invariant. Homotopy invariance only holds after passing to homology. As has been realized by participants of the Special Year on Univalent Foundations of Mathematics at the IAS, the observation that the cohomology functors are represented by Eilenberg--MacLane spaces can be used to define the cohomology groups in the setting of homotopy type theory. To define cohomology groups with coefficients in an abelian group~\(G\) using this idea one must first construct an Eilenberg--MacLane type for~\(G\) in all degrees. Dan Licata and Eric Finster have shown~\cite{EilenbergMacLaneSpacesInHoTT} that this is always possible by describing how to construct an Eilenberg--MacLane type for any abelian group~\(G\) and degree~\(n:\NN{}\). In this chapter we first review their construction and then describe more formally how to use the Eilenberg--MacLane types to define cohomology groups.

\begin{definition}
A group \(G\) is a tuple (dependent pair type) consisting of
\begin{itemize}
\item an underlying set, which we also write~\(G\),
\item a distinguished value \(e_G : G\), the \emph{unit},
\item an infix function \({\groupOp} : G \to G \to G\), the group \emph{multiplication},
\item an unary function \((\blank)^{-1} : G \to G\), the \emph{inverse function}, together with
\item a proof that \(e_G\) really is a left unit, that is a value of the type \(\prod_{y:G} e_G \groupOp y = y\),
\item a proof of \emph{associativity} of~\({\groupOp}\), that is a dependent function
\[\groupAssoc{G} : \prod_{g,h,k:G} (g \groupOp h) \groupOp k = g \groupOp (h \groupOp k),\]
\item and a proof that \(\prod_{g:G} g^{-1} \groupOp g = e_G\).
\end{itemize}
From this, we can show that \(e_G\) is also a right unit and that \(\prod_{g:G} g \groupOp g^{-1} = e_G\) holds. We call a group~\(G\) \emph{abelian} if we additionally have a proof
\[\isAbelian{G} \enspace:\enspace \prod_{g,h:G} g \groupOp h = h \groupOp g.\]
For such groups, we use \({\abOp}\) instead of~\({\groupOp}\) and \(-g\) instead of \(g^{-1}\).
\end{definition}

\begin{definition}
A \emph{homomorphism} \(\varphi{}\) between groups \(G\) and \(H\) is a pair consisting of
\begin{itemize}
\item A function \(G \to H\) between the underlying sets, for which also use \(\varphi{}\), and
\item a proof that for each \(g_1, g_2 : G\), \(\varphi(g_1 \groupOp g_2) = \varphi(g_1) \groupOp \varphi(g_2)\).
\end{itemize}
A homomorphism \(\varphi{}\) is an \emph{isomorphism} if its underlying function is an equivalence. In this case, one can show that the inverse function is also a homomorphism.
\end{definition}

Let \(G\) be a group.

\begin{definition}[{\cite[][Definition III.1]{EilenbergMacLaneSpacesInHoTT}}]
The \emph{\EilenbergMacLane{} space} \(\K{G}{1}\) is the higher inductive 1-type defined by
\begin{itemize}
\item \(\embase : \K{G}{1}\)
\item \(\emloop : G \to (\embase =_{\K{G}{1}} \embase)\)
\item \(\emloopComp : \prod_{g,h:G} \emloop(g \groupOp h) = \concat{\emloop(g)}{\emloop(h)} \)
\end{itemize}
When we regard \(\K{G}{1}\) as a pointed type, the implicit base point is~\(\embase{}\).
\end{definition}

\begin{remark}\label{rem:loop-ident}
The paper by Dan Licata and Eric Finster also included the constructor
\begin{itemize}
\item \(\emloopIdent : \emloop(e_G) = \refl{\embase}\)
\end{itemize}
Instead of adding this as a constructor, we derive \(\emloopIdent{}\) from
\[\begin{tikzcd}[column sep=2.5cm]
\emloop(e_G) \arrow[r, equal, "\ap{\emloop}(p)"] &
\emloop(e_G \groupOp e_G) \arrow[r, equal, "{\emloopComp(e_G,~e_G)}"] &
\concat{\emloop(e_G)}{\emloop(e_G)}
\end{tikzcd}\]
where \(p : e_G = e_G \groupOp e_G\) by cancelling \(\emloop(e_G)\) on both sides (composition with a fixed path is an equivalence).
Omitting this 2-path constructor makes no difference since \(\K{G}{1}\) is defined as a 1-type. Hence, the points and 1-paths in the definition can be interpreted as generators, while the 2-paths are relations between the generators. What we have done is to simply leave out a relation that can be deduced from the other relations.
\end{remark}

From this, we can deduce:
\begin{lemma}\label{lem:inverse-emloop}
For all \(g:G\), \(\emloop(g^{-1}) = \inverse{\emloop(g)}\).
\end{lemma}

\begin{proof}
We calculate
\[\begin{array}{cl}
& \concat{\emloop(g)}{\emloop(g^{-1})} \\
& \reason{5cm}{\(\inverse{\emloopComp(g, g^{-1})}\)} \\
= & \emloop(g \groupOp g^{-1}) \\
& \reason{5cm}{by the group laws} \\
= & \emloop(e_G) \\
& \reason{5cm}{\(\emloopIdent{}\)} \\
= & \refl{\embase} \\
& \reason{5cm}{\(\inverse{\inverseR(\emloop(g))}\)} \\
= & \concat{\emloop(g)}{\inverse{\emloop(g)}}.
\end{array}\]
We arrive at the statement by cancelling \(\emloop(g)\).
\end{proof}

The induction principle for this type states that given any dependent family \(C : \K{G}{1} \to \UU\) such that every \(C(x)\) is a \(1\)-type,
\begin{itemize}
\item a point \enspace{}\(c : C(\embase)\),
\item a family of dependent 1-paths \enspace{}\(\ell : \prod_{g:G} \depid{C}{\emloop(g)}{c}{c}\), and
\item a family of dependent 2-paths \enspace{}\(\ell\defname{\mhyphen{}comp} : \prod_{g,h:G} \depid{\lam{p}[\embase = \embase] \depid{C}{p}{c}{c}}{\emloopComp(g, h)}{\ell(g \groupOp h)}{\depConcat{\ell(x)}{\ell(y)}}\)
\end{itemize}
there is a dependent function \(f : \prod_{x:\K{G}{1}} C(x)\) such that
\begin{itemize}
\item \(f(\embase) \jeq c\) and
\item \(\apd{f}(\emloop(g)) = \ell(g)\) for all \(g:G\).
\end{itemize}

As a first application, let us prove

\begin{lemma}\label{lem:kg1-0-connected}
\(\K{G}{1}\) is 0-connected.
\end{lemma}

\begin{proof}
We have to show that \(\trunc{\K{G}{1}}{0} \) is contractible. As the center of contraction we choose \(\truncProj{\embase}{0}\). It remains to show that \(\truncProj{\embase}{0} = z \) for all \(z:\trunc{\K{G}{1}}{0}\). To do this we first use \(\trunc{\blank}{0}\)-induction to get some \(\tilde{z}:\K{G}{1}\) for which we have to show \(\truncProj{\embase}{0} = \truncProj{\tilde{z}}{0}\). We now induct on~\(\tilde{z}\). The case in which \(\tilde{z}\) is \(\embase\) is trivial (reflexivity) as are the other cases since \(\truncProj{\embase}{0} = \truncProj{\tilde{z}}{0}\) is a family of mere propositions.
\end{proof}

\begin{remark}\label{rem:kg1-induction-principle-1-type}
We can now turn around and use this to simplify the induction principle of~\(\K{G}{1} \). Since being a 1-type is a mere proposition and \(\K{G}{1} \) is 0-connected it suffices to check that~\(C(\embase)\) is a 1-type. This follows from~\cite[][Lemma 7.5.7]{HoTTBook} (using the base point inclusion function as~\(f\)) in combination with~\cite[][Lemma 7.5.11]{HoTTBook}.
\end{remark}

The recursion principle for \(\K{G}{1}\) says that a function \(f : \K{G}{1} \to C\) to a 1-type~\(C\) can be specified by giving
\begin{itemize}
\item a point \enspace{}\(c : C\),
\item a family of 1-paths \enspace{}\(\ell : G \to c = c\), and
\item a family of 2-paths \enspace{}\(\ell\defname{\mhyphen{}comp} : \prod_{g,h:G} \ell(g \groupOp h) = \concat{\ell(x)}{\ell(y)}\).
\end{itemize}
The function \(f\) then satisfies
\begin{itemize}
\item \(f(\embase) \jeq c\) and
\item \(\ap{f}(\emloop(g)) = \ell(g)\) for all \(g:G\).
\end{itemize}

\begin{definition}
For every group homomorphism \(\varphi : G \to H\) there is a (pointed) map \(\Kfmap{\varphi}{1} : \K{G}{1} \to \K{H}{1}\) with
\begin{itemize}
\item \(\Kfmap{\varphi}{1}[\embase] \deq \embase\) and
\item \(\ap{\Kfmap{\varphi}{1}}(\emloop(g)) = \emloop(\varphi(g))\) for all \(g : G\).
\end{itemize}
This is compatible with composition since for each \(g_1, g_2 : G\) there is a path
\[\begin{tikzcd}[column sep=2cm]
\emloop(\varphi(g_1 \groupOp g_2))
  \arrow[r, equal, "{\ap{\emloop}(\homPresComp{\varphi}(g_1, g_2))}"{yshift=0.2cm}] &
\emloop(\varphi(g_1) \groupOp \varphi(g_2))
  \arrow[r, equal, "{\emloopComp(\varphi(g_1), \varphi(g_2))}"{yshift=0.2cm}] &
\concat{\emloop(\varphi(g_1))}{\emloop(\varphi(g_2))}.
\end{tikzcd}\]
\end{definition}

% in HoTT-Agda: EM₁-fmap-idhom
% in HoTT-Agda: ⊙EM₁-fmap-idhom
% in HoTT-Agda: EM₁-fmap-∘
% in HoTT-Agda: ⊙EM₁-fmap-∘
\begin{lemma}\label{lem:em1-functorial}
This definition is functorial:
\begin{enumerate}
\item[(a)] \(\Kfmap{\idHom(G)}{1} = \idfat{\K{G}{1}}\) as pointed maps for all groups~\(G\).
\item[(b)] \(\Kfmap{\psi \circ \varphi}{1} = \Kfmap{\psi}{1} \circ \Kfmap{\varphi}{1}\) as pointed maps for all groups \(G\), \(H\), \(K\) and group homomorphisms \(\varphi : G \to H\) and \({\psi : H \to K}\).
\end{enumerate}
\end{lemma}

\begin{proof}
To prove each statement, we first construct a homotopy~\(h\) between the underlying maps using \(\K{G}{1}\)-induction. Since the witness for base point preservation of all four functions on both sides of ``\(=\)'' in (a) and (b) is the identity path, as will be \(h(\embase)\), we get a path between the pointed maps by \Cref{rem:path-between-pointed-maps}.
\begin{enumerate}
\item[(a)] We construct for each \(x:\K{G}{1}\) a path \(h(x) : \Kfmap{\idHom(G)}{1}[x] = x\) by induction on~\(x\). For \(x \jeq \embase{}\) we set \(h(\embase) \deq \refl{\embase}\). In the \(\emloop{}\)-case we have to construct for each \(g:G\) a dependent path
\[\depid{\lam{x} \Kfmap{\idHom(G)}{1}[x] = x}{\emloop(g)}{\refl{\embase}}{\refl{\embase}}.\]
This dependent path type is equivalent to the type of commutative square
\[\begin{tikzcd}[column sep={5cm,between origins}, row sep={2cm,between origins}]
\embase
  \arrow[r, equals, "\ap{\Kfmap{\idHom(G)}{1}}(\emloop(g))"]
  \arrow[d, equals, "\refl{\embase}"'] &
\embase
  \arrow[d, equals, "\refl{\embase}"] \\
\embase
  \arrow[r, equals, "\ap{\idfat{\K{G}{1}}}(\emloop(g))"] &
\embase
\end{tikzcd}\]
by \Cref{lem:dep-path-in-path-type}. This square commutes because
\[\begin{array}{cl}
& \ap{\Kfmap{\idHom(G)}{1}}(\emloop(g)) \\
& \reason{5cm}{\(\beta{}\) rule of \(\Kfmap{\idHom(G)}{1}\)} \\
= & \emloop(\idHom(G, g)) \\
\jeq & \emloop(g) \\
& \reason{5cm}{by~\cite[][Lemma 2.2.2 (iv)]{HoTTBook}} \\
= & \ap{\idfat{\K{G}{1}}}(\emloop(g)).
\end{array}\]
The \(\emloopComp{}\) case is trivial since \(\lam{x} \Kfmap{\idHom(G)}{1}[x] = x\) is a family of sets.
\item[(b)] We construct for each \(x:\K{G}{1}\) a path \(h(x) : \Kfmap{\psi}{1}[\Kfmap{\varphi}{1}[x]] = \Kfmap{\psi \circ \varphi}{1}[x]\) by induction on~\(x\). For \(x \jeq \embase{}\) we set \(h(\embase) \deq \refl{\embase}\). In the \(\emloop{}\)-case we have to construct for each \(g:G\) a dependent path
\[\depid{\lam{x} \Kfmap{\psi}[\Kfmap{\varphi}{1}[x]] = \Kfmap{\psi \circ \varphi}{1}[x]}{\emloop(g)}{\refl{\embase}}{\refl{\embase}}.\]
This dependent path type is equivalent to the type of commutative square
\[\begin{tikzcd}[column sep={5cm,between origins}, row sep={2cm,between origins}]
\embase
  \arrow[r, equals, "\ap{\Kfmap{\psi}{1} \circ \Kfmap{\varphi}{1}}(\emloop(g))"]
  \arrow[d, equals, "\refl{\embase}"'] &
\embase
  \arrow[d, equals, "\refl{\embase}"] \\
\embase
  \arrow[r, equals, "\ap{\Kfmap{\psi \circ \varphi}{1}}(\emloop(g))"] &
\embase
\end{tikzcd}\]
by \Cref{lem:dep-path-in-path-type}. This square commutes because
\[\begin{array}{cl}
& \ap{\Kfmap{\psi}{1} \circ \Kfmap{\varphi}{1}}(\emloop(g)) \\
& \reason{5cm}{\(\apFunctoriality(\Kfmap{\psi}{1}, \Kfmap{\varphi}{1}, \emloop(g))\)} \\
= & \ap{\Kfmap{\psi}{1}}(\Kfmap{\varphi}{1}(\emloop(g))) \\
& \reason{5cm}{\(\beta{}\) rule for \(\Kfmap{\varphi}{1}\)} \\
= & \ap{\Kfmap{\psi}{1}}(\emloop(\varphi(g))) \\
& \reason{5cm}{\(\beta{}\) rule for \(\Kfmap{\psi}{1}\)} \\
= & \emloop(\psi(\varphi(g))) \\
& \reason{5cm}{\(\beta{}\) rule for \(\Kfmap{\psi \circ \varphi}{1}\)} \\
= & \ap{\Kfmap{\psi \circ \varphi}{1}}(\emloop(g)).
\end{array}\]
The \(\emloopComp{}\) case is trivial since
\[\lam{x} \Kfmap{\psi}{1}[\Kfmap{\varphi}{1}[x]] = \Kfmap{\psi \circ \varphi}{1}[x]\]
is a family of sets.\qedhere{}
\end{enumerate}
\end{proof}

Next, we prove that the type \(\K{G}{1}\) really deserves its name by having fundamental group~\(G\) and all other homotopy groups vanishing. Let \(\phi{}\) be the composition in the following diagram.

\[\begin{tikzcd}
G \arrow[r, "\emloop"] \arrow[rr, bend right=10, "\phi"{below}] &
\Omega(\K{G}{1}) \arrow[r, "\truncProj{\blank}{0}"] &
\trunc{\Omega(\K{G}{1})}{0} \jeq \pi_1(\K{G}{1})
\end{tikzcd}\]

\begin{theorem}[{\cite[cf.][Theorem III.2]{EilenbergMacLaneSpacesInHoTT}}]\label{thm:kg1-fundamental-group}
\(\phi \) is a group isomorphism.
\end{theorem}

We will show details of the proof since later, after modifying the definition of~\(\K{G}{1}\), we will be required to fix it. The main part of the proof is the following proposition:

\begin{proposition}\label{prop:emloop-is-equiv}
\(\emloop \) is an equivalence.
\end{proposition}

The proof of this proposition uses what is known as the encode-decode method, which has first been used to prove that the fundamental group of the circle is~\(\ZZ \) in~\cite[][Section 8.1.4]{HoTTBook}.

\begin{proof}[Proof of \Cref{prop:emloop-is-equiv}]
\textsc{Step 1.}
We begin by defining a type family \(\Codes : \K{G}{1} \to \Set \) using recursion on~\(\K{G}{1}\) with
\[\begin{array}{r c l}
  \Codes(\embase) &\deq& G \\
  \ap{\Codes}(\emloop(g)) &=& \ua(\blank \groupOp g)
\end{array}\]
where \(\blank \groupOp g \) is the equivalence given by sending each \(h:G\) to \(h \groupOp g\); its inverse is \(\blank \groupOp g^{-1}\) by the group laws.
As required by the recursion principle, we give the following 2-path as a witness that our definition is compatible with composition for \(g_1, g_2 : G\):
\[\begin{array}{c l}
& \concat{\ua(\blank \groupOp g_1)}{\ua(\blank \groupOp g_2)} \\
& \reason{10cm}{compatibility of the univalence axiom with composition of equivalences (see~\cite[][Section 2.10]{HoTTBook})} \\
= & \ua((\blank \groupOp g_2) \ecomp (\blank \groupOp g_1)) \\
\jeq & \ua((\blank \groupOp g_1) \groupOp g_2) \\
& \reason{10cm}{associativity of~\(\groupOp \) and function extensionality} \\
= & \ua(\blank \groupOp (g_1 \groupOp g_2))
\end{array}\]
For later use, we define \(\transportCodes(g, h)\) for \(g, h : G\) as the composite path
\[\begin{array}{c l}
& \transport{\Codes}{\emloop(g)}[h] \\
& \reason{10cm}{see \cite[][Lemma 2.3.10]{HoTTBook}} \\
= & \transport{\lam{X} X}{\ap{\Codes}(\emloop(g))}[h] \\
& \reason{10cm}{definition of \(\Codes\)} \\
= & \transport{\lam{X} X}{\ua(\blank \groupOp g)}[h] \\
& \reason{10cm}{propositional computation rule of \(\ua{}\) (see~\cite[][Section 2.10]{HoTTBook})} \\
= & h \groupOp g
\end{array}\]

\textsc{Step 2.}
We define \(\encode : \prod_{z:\K{G}{1}} \embase = z \to \Codes(z) \) by
\[ \encode(p) \deq \transport{\Codes}{p}[e_G] \]
and \(\decode : \prod_{z:\K{G}{1}} \Codes(z) \to \embase = z \) using induction on \(z:\K{G}{1} \) with
\[ \decode(\embase) \deq \emloop. \]
The induction principle requires us to give dependent 1-paths \(\emloop =_{\emloop(g)}^{\lam{z} \Codes(z) \to \embase = z} \emloop \) for all \(g:G\). This follows from \Cref{lem:dep-path-in-arrow-type} about dependent paths in function types applied to the following chain of equations:
% there's a copy of this
\[\begin{array}{c l}
& \transport{\lam{z} \embase = z}{\emloop(g)}[\emloop(h)] \\
& \reason{5cm}{\(\covTransport(\emloop(h), \emloop(g))\)} \\
= & \concat{\emloop(h)}{\emloop(g)} \\
& \reason{5cm}{\(\inverse{\emloopComp(h, g)}\)} \\
= & \emloop(h \groupOp g) \\
& \reason{5cm}{\(\ap{\emloop}(\inverse{\transportCodes(g, h)})\)} \\
= & \emloop(\transport{\Codes}{\emloop(g)}[h])
\end{array}\]
Providing the required dependent 2-paths is trivial: Since \(\K{G}{1}\) is a 1-type, \(\lam{z} \Codes(z) \to \embase = z\) is a family of sets. Hence all dependent 2-path types in this family are contractible.

\textsc{Step 3.}
We now prove that \(\encode(\embase)\) and \(\decode(\embase)\) are mutually inverse.
To show that one composition of these functions is the identity, we calculate
\[\begin{array}{c l}
& \encode(\embase, \decode(\embase, g)) \\
\jeq & \encode(\embase, \emloop(g)) \\
\jeq & \transport{\Codes}{\emloop(g)}[e_G] \\
& \reason{5cm}{\(\transportCodes(g, e_G)\)} \\
= & e_G \groupOp g \\
& \reason{5cm}{\(e_G\) is a left unit} \\
= & g
\end{array}\]
for \(g:G\). For the other composition, we prove the more general statement
\[ \prod_{z:\K{G}{G}} \prod_{p : \embase = z} \decode(z, \encode(z, p)) = p. \]
By path induction, we may assume that \(p\) is reflexivity and \(z\) is \(\embase\). The goal now becomes \(\emloop(e_G) = \refl{\embase}\), which is exactly the type of \(\emloopIdent\).
\end{proof}

The reason for defining \(\encode{}\) and \(\decode{}\) as fiberwise transformations lies in the last step: If we had instead defined these as functions between \(\embase = \embase{}\) and \(G\) we would not have been able to use path induction on~\(p\).

\begin{proof}[Proof of \Cref{thm:kg1-fundamental-group}]
The proposition implies that \(\Omega(\K{G}{1}\) is a set because it is equivalent to the set~\(G\).
Therefore the map \(\truncProj{\blank}{0} : \Omega(\K{G}{1}) \to \trunc{\Omega(\K{G}{1})}{0}\) is also an equivalence. (A possible proof of general fact that the projection map from an \(n\)-type to its \(n\)-truncation is an equivalence is similar to that of~\cite[][Lemma 7.3.15]{HoTTBook}.)
Hence, \(\phi \) is an equivalence.

The map \(\phi \) is also a group homomorphism:
\[\begin{array}{c l}
& \phi(g \groupOp h) \\
\jeq & \truncProj{\emloop(g \groupOp h)}{0} \\
& \reason{5cm}{\(\ap{\truncProj{\blank}{0}}(\emloopComp(g, h))\)} \\
= & \truncProj{\concat{\emloop(g)}{\emloop(h)}}{0} \\
\jeq & \truncConcat{0}{\truncProj{\emloop(g)}{0}}{\truncProj{\emloop(h)}{0}} \\
\jeq & \truncConcat{0}{\phi(g)}{\phi(h)}
\end{array}\]

This concludes our proof, since a group homomorphism is an isomorphism if and only if it is an equivalence.
\end{proof}

\begin{definition}[{\cite[][Definition IV.1]{EilenbergMacLaneSpacesInHoTT}}]
Given a pointed type \((A, a_0)\), a \emph{coherent \hStructure{}} on \((A, a_0)\) consists of
\begin{itemize}
\item an operation \(\hOp : A \to A \to A\),
\item a \emph{left unitor} \(\hUnitL : \prod_{a:A} a_0 \hOp a = a\),
\item a \emph{right unitor} \(\hUnitR : \prod_{a:A}a \hOp a_0 = a\), and
\item a \emph{coherence} \(\hUnitCoh : \hUnitL(a_0) = \hUnitR(a_0)\).
\end{itemize}
\end{definition}

\begin{lemma}
Let a coherent \hStructure{} on \((A, a_0)\) where \(A\) is 0-connected be given.
Then \(a \hOp \blank\) is an equivalence for every \(a:A\).
\end{lemma}

\begin{proof}
Since \(A\) is 0-connected and being an equivalence is a mere proposition, it suffices to show that \(a_0 \hOp \blank\) is an equivalence. But \(a_0 \hOp \blank\) is by \(\hUnitL\) and function extensionality equal to \(\idfat{A}\) which is an equivalence.
\end{proof}

\begin{remark}
Licata and Finster's definition also requires a proof that \(a \hOp \blank\) is an equivalence for all~\(a:A\). Since we are only going to consider \hStructure{}s on 0-connected types, we don't need to include this property in the definition of an \hStructure{}.
\end{remark}

\begin{lemma}[{\cite[][Lemma IV.2]{EilenbergMacLaneSpacesInHoTT}}]
If \(G\) is abelian, then there is an \hStructure{} on \(\K{G}{1}\).
\end{lemma}

\begin{proof}
We define \(\hOp : \K{G}{1} \to \K{G}{1} \to \K{G}{1}\) by \(\K{G}{1}\)-recursion on the first argument.
\begin{itemize}
\item For the \(\embase\) case we set \((\embase \hOp \blank) \deq \idfat{\K{G}{1}}\).
\item For every \(g:G\) we must give a path of type \(\idfat{\K{G}{1}} = \idfat{\K{G}{1}}\) for \(\ap{\lam{x} \lam{y} x \hOp y}(\emloop(g))\). We construct this as \(\funext(\hOpLoop(g))\) where the homotopy \(\hOpLoop(g) : \idfat{\K{G}{1}} \sim \idfat{\K{G}{1}}\) is defined by \(\K{G}{1}\)-induction as follows:
\begin{itemize}
\item In the base case we use \(\hOpLoop(g, \embase) \deq \emloop(g)\).
\item Now we have to construct for each \(h:G\) a dependent path \(\depid{\lam{x} x = x}{\emloop(h)}{\emloop(g)}{\emloop(g)}\). By~\cite[][Theorem 2.11.5]{HoTTBook} it suffices to calculate
\[\begin{array}{cl}
& \concat{\emloop(g)}{\emloop(h)} \\
& \reason{5cm}{\(\inverse{\emloopComp(g, h)}\)} \\
= & \emloop(g \groupOp h) \\
& \reason{5cm}{\(\ap{\emloop}(\isAbelian{G}(g, h))\)} \\
= & \emloop(h \groupOp g) \\
& \reason{5cm}{\(\emloopComp(h, g)\)} \\
= & \concat{\emloop(h)}{\emloop(g)}
\end{array}\]
\item Since \(\lam{x:\K{G}{1}} x = x\) is a family of sets, compability with composition is automatic.
\end{itemize}
\item Finally, we must prove compatibility with composition of \(g_1,g_2:G\). We have to show that \(\funext(\hOpLoop(g_1 \groupOp g_2)) = \concat{\funext(\hOpLoop(g_1))}{\funext(\hOpLoop(g_2))}\). By \Cref{lem:funext-pres-comp} the right-hand side is equal to \(\funext(\lam{x} \concat{\hOpLoop(g_1, x)}{\hOpLoop(g_2, x)})\). Therefore it suffices to exhibit a path of type \(\hOpLoop(g_1 \groupOp g_2) = \lam{x} \concat{\hOpLoop(g_1, x)}{\hOpLoop(g_2, x)}\). By function extensionality this is equivalent to defining a path \(\hOpLoop(g_1 \groupOp g_2, x) = \concat{\hOpLoop(g_1, x)}{\hOpLoop(g_2, x)}\) for every \(x:\K{G}{1}\). Since this family of path types is a family of mere propositions and \(\K{G}{1}\) is 0-connected, we only have to give such a path for \(x \jeq \embase\). In this special case, the target type becomes \(\emloop(g_1 \groupOp g_2) = \concat{\emloop(g_1)}{\emloop(g_2)}\), which is exactly the type of~\(\emloopComp(g_1, g_2)\).
\end{itemize}
Next, we define for \(g:G\) and \(y:\K{G}{1}\) the composite path
\[\hOpLoopBeta(g, y) \deq
\left\{\enspace\begin{array}{cl}
& \ap{\blank \hOp y}(\emloop(g)) \\
& \reason{6.5cm}{\(\apCirc((\lam{f} f(y)), (\lam{x} \lam{y} x \hOp y), \emloop(g))\)} \\
= & \happly(\ap{\lam{x} \lam{y} x \hOp y}(\emloop(g)), y) \\
& \reason{6.5cm}{definition of \({\hOp}\)} \\
= & \happly(\funext(\hOpLoop(g)), y) \\
& \reason{6.5cm}{\(\happly(\equivEpsilon{\happly}(\hOpLoop(g)), y)\)} \\
= & \hOpLoop(g, y).
\end{array}\right.\]
The neutral element is~\(\embase{}\). By definition, we have \(\embase \hOp x \jeq x\) for all \(x:\K{G}{1}\).
We prove the other identity \(x \hOp \embase = x\) for each \(x:\K{G}{1}\) by induction on~\(x\).
\begin{itemize}
\item If \(x\) is \(\embase{}\), both sides reduce to \(\embase{}\), and we are done by~\(\refl{\embase}\).
\item We now have to construct for each \(g:G\) a dependent path of type
\[\depid{\lam{x} x \hOp \embase = x}{\emloop(g)}{\refl{\embase}}{\refl{\embase}}.\]
By~\Cref{lem:dep-path-in-path-type} this dependent path type is equivalent to the ordinary path type
\[\concat{\refl{\embase}}{\ap{\idfat{\K{G}{1}}}(\emloop(g))}
\enspace=\enspace
\concat{\ap{\blank \hOp \embase}(\emloop(g))}{\refl{\embase}}.\]
We calculate
\[\begin{array}{c l}
& \concat{\refl{\embase}}{\ap{\idfat{\K{G}{1}}}(\emloop(g))} \\
\jeq & \ap{\idfat{\K{G}{1}}}(\emloop(g)) \\
& \reason{5cm}{by~\cite[][Lemma 2.2.2 (iv)]{HoTTBook}} \\
= & \emloop(g) \\
\jeq & \hOpLoop(g, \embase) \\
& \reason{5cm}{\(\inverse{\hOpLoopBeta(g, \embase)}\)} \\
= & \ap{\blank \hOp \embase}(\emloop(g)) \\
& \reason{6cm}{since reflexivity is a unit} \\
= & \concat{\ap{\blank \hOp \embase}(\emloop(g))}{\refl{\embase}}.
\end{array}\]
\end{itemize}
These two unit laws are coherent by reflexivity.
\end{proof}

\begin{definition}
Let \(G\) be an abelian group and \(n : \NN{}\). The \emph{\EilenbergMacLane{} space} \(\K{G}{0}\) is~\(G\). For \(n > 1\), the \emph{\EilenbergMacLane{} space} \(\K{G}{n}\) is defined as \(\trunc{\iterSusp{n-1}{\K{G}{1}}}{n}\).
When we regard an \EilenbergMacLane{} space as a pointed type, the basepoint is
\[\begin{array}{l l}
e_G, & \text{if \(n = 0\),} \\
\embase, & \text{if \(n = 1\), and} \\
\truncProj{\suspNorth}{n}, & \text{if \(n > 1\).}
\end{array}\]
\end{definition}

\begin{definition}
Let \(\varphi : G \to H\) be a homomorphism of abelian groups and \(n:\NN{}\). We define \(\Kfmap{\varphi}{n} : \K{G}{n} \pto \K{H}{n}\) by case analysis:
\begin{itemize}
\item If \(n = 0\) we simply take the underlying map of \(\varphi{}\). (This is a pointed map since every group homomorphism preserves the neutral element.)
\item For \(n = 1\) we have already defined \(\Kfmap{\varphi}{n}\).
\item For \(n = 1+n'\) with \(n' \geq 1\) we set \(\Kfmap{\varphi}{n} \deq \truncFmap{\iterSuspFmap{n'}{\Kfmap{\varphi}{1}}}{n}\).
\end{itemize}
\end{definition}

% in HoTT-Agda: EM-fmap-idhom
% in HoTT-Agda: ⊙EM-fmap-idhom
% in HoTT-Agda: EM-fmap-∘
% in HoTT-Agda: ⊙EM-fmap-∘
\begin{lemma}\label{lem:em-functorial}
The mapping \(\varphi \mapsto \Kfmap{\varphi}{n}\) is functorial:
\begin{enumerate}
\item[(a)] \(\Kfmap{\idHom(G)}{n} = \idfat{\K{G}{n}}\) as pointed maps for all abelian groups~\(G\) and~\(n:\NN{}\).
\item[(b)] \(\Kfmap{\psi \circ \varphi}{n} = \Kfmap{\psi}{n} \circ \Kfmap{\varphi}{n}\) as pointed maps for all groups \(G\), \(H\), \(K\), group homomorphisms \(\varphi : G \to H\) and \({\psi : H \to K}\) and \(n:\NN{}\).
\end{enumerate}
\end{lemma}

\begin{proof}
For \(n=0\) both equalities hold judgementally. For \(n=1\) the statement is exactly the statement of \Cref{lem:em1-functorial}. For \(n=1+n'\) (with \(n' \geq 1\)) we calculate:
\[\begin{array}[b]{ccl}
\text{(a)\qquad} && \Kfmap{\idHom(G)}{1+n'} \\
& \jeq & \truncFmap{\iterSuspFmap{n'}{\Kfmap{\idHom(G)}{1}}}{1+n'} \\
& & \reason{3.5cm}{by~\Cref{lem:em1-functorial}~(a)} \\
& = & \truncFmap{\iterSuspFmap{n'}{\idfat{\K{G}{1}}}}{1+n'} \\
& & \reason{3.5cm}{by~\Cref{lem:iter-susp-functorial}~(a)} \\
& = & \truncFmap{\idfat{\iterSusp{n'}{\K{G}{1}}}}{1+n'} \\
& & \reason{3.5cm}{by~\Cref{lem:trunc-fmap-functorial}~(a)} \\
& = & \idfat{\trunc{\iterSusp{n'}{\K{G}{1}}}{1+n'}} \\[0.5cm]
\text{(b)\qquad} && \Kfmap{\psi \circ \varphi}{1+n'} \\
& \jeq & \truncFmap{\iterSuspFmap{n'}{\Kfmap{\psi \circ \varphi}{1}}}{1+n'} \\
& & \reason{3.5cm}{by~\Cref{lem:em1-functorial}~(b)} \\
& = & \truncFmap{\iterSuspFmap{n'}{\Kfmap{\psi}{1} \circ \Kfmap{\varphi}{1}}}{1+n'} \\
& & \reason{3.5cm}{by~\Cref{lem:iter-susp-functorial}~(b)} \\
& = & \truncFmap{\iterSuspFmap{n'}{\Kfmap{\psi}{1}} \circ \iterSuspFmap{n'}{\Kfmap{\varphi}{1}}}{1+n'} \\
& & \reason{3.5cm}{by~\Cref{lem:trunc-fmap-functorial}~(b)} \\
& = & \truncFmap{\iterSuspFmap{n'}{\Kfmap{\psi}{1}}}{1+n'} \circ \truncFmap{\iterSuspFmap{n'}{\Kfmap{\varphi}{1}}}{1+n'} \\
& \jeq & \Kfmap{\psi}{1+n'} \circ \Kfmap{\varphi}{1+n'}
\end{array}\qedhere{}\]
\end{proof}

% in HoTT-Agda: transport-EM
% in HoTT-Agda: transport-EM-uaᴬᴳ
% in HoTT-Agda: ⊙transport-⊙EM
% in HoTT-Agda: ⊙transport-⊙EM-uaᴬᴳ
\begin{corollary}\label{lem:transport-em}
Let \(G\) and \(H\) be abelian groups and \(n:\NN{}\).
\begin{enumerate}
\item[(a)] For every path \(p : G = H\) there is a path of pointed maps
\[\transport{\K{\blank}{n}}{p} = \Kfmap{\coerceAbGroup(p)}{n}.\]
where \(\uaAbGroup : (G \cong H) \to (G \to H)\) turns a path between abelian groups to a homomorphism between them (see the formalization for the exact definition of this function).
\item[(b)] For every isomorphism \(\varphi : G \cong H\) there is a path
\[\transport{\K{\blank}{n}}{\uaAbGroup(\varphi)} = \Kfmap{\varphi}{n}\]
where \(\uaAbGroup : (G \cong H) \to (G = H)\) uses univalence to turn an isomorphism between abelian groups into a path between them (see the formalization for the exact definition of this function).
\end{enumerate}
\end{corollary}

\begin{proof}
\begin{enumerate}
\item[(a)] By path induction, we may assume that \(p\) is~\(\refl{G}\). The left-hand side now reduces to \(\idfat{\K{A}{n}}\) while the right-hand side reduces to \(\Kfmap{\idfat{G}}{n}\). These are equal by \Cref{lem:em-functorial}~(a).
\item[(b)] By (a) and a computation rule for \(\coerceAbGroup{}\) and \(\uaAbGroup{}\) which is similar to and, in fact, follows from the computation rule \(\coerce(\ua(f)) = f\) for equivalences~\(f\), we have
\[\transport{\K{\blank}{n}}{\uaAbGroup(\varphi)} = \Kfmap{\coerceAbGroup(\uaAbGroup(\varphi))}{n} = \Kfmap{\varphi}{n}.\qedhere{}\]
\end{enumerate}
\end{proof}

We turn to the problem of calculating the homotopy groups of \EilenbergMacLane{} spaces. Since higher \EilenbergMacLane{} spaces are defined using iterated suspension, the following fundamental theorem will be useful:

\begin{theorem}[The Freudenthal suspension theorem, {\cite[][Theorem 8.6.4]{HoTTBook}}]\label{thm:freudenthal-suspension}
Suppose that \(X\) is \(n\)-connected and pointed, with \(n \geq 0\). Then the map \(\eta : X \to \LoopSpace{\susp{X}}\) is \(2n\)-connected.
\end{theorem}

\begin{corollary}\label{cor:freudenthal-equivalence}
If \(X\) is \(n\)-connected and pointed, with \(n \geq 0\), then
\[\truncFmap{\eta}{k} \quad:\quad \trunc{X}{k} \to \trunc{\LoopSpace{\susp{X}}}{k}\]
is an equivalence for all \(k \leq 2 n\).
\end{corollary}

\begin{proof}
This follows from the Freudenthal suspension theorem using~\cite[][Corollary 7.5.14]{HoTTBook} and the fact that an \(m\)-connected map is \(k\)-connected for every \(k \leq m\).
\end{proof}

The next theorem shows that the claim of the corollary holds also for \(k \jeq 1\) and \(n \jeq 0\), given that \(X\) satisfies an additional precondition.

\begin{theorem}\label{thm:hstructure-suspension-pi2}
Let \(X\) be a pointed, 0-connected 1-type with an \hStructure{}. Then
\[\parens*{\lam{x} \truncProj{\eta(x)}{1}}\quad:\quad
X \to \trunc{\LoopSpace{\susp{X}}}{1}\]
is an equivalence.
\end{theorem}

\begin{proof}
This is the ``main lemma'' in the proof of~\cite[][Theorem IV.3]{EilenbergMacLaneSpacesInHoTT}.
\end{proof}

\begin{theorem}\label{thm:em-spectrum}
For all abelian groups \(G\) and \(n : \NN{}\), there is a pointed equivalence
\[\K{G}{n} \equiv \LoopSpace{\K{G}{1+n}}.\]
\end{theorem}

\begin{corollary}[{\cite[][cf. Theorem V.4]{EilenbergMacLaneSpacesInHoTT}}]\label{cor:em-properties}
For all abelian groups \(G\) and \(n : \NN{}\), the \EilenbergMacLane{} space \(\K{G}{n}\) is
\begin{itemize}
\item \((n-1)\)-connected (and therefore \(\pi_k(\K{G}{n}) = \trivialGroup\) for \(k < n\)) and
\item \(n\)-truncated (and therefore \(\pi_k(\K{G}{n}) = \trivialGroup\) for \(k > n\))
\end{itemize}
where \(\trivialGroup{}\) denotes the trivial group (and for \(k = 0\), the unit type \(\unitType{}\)). The only potentially non-vanishing homotopy group is \(\pi_n(\K{G}{n}) = G\).
\end{corollary}

Hence, the \EilenbergMacLane{} spaces form what is called a \emph{spectrum}, a sequence of pointed types (or pointed spaces in classical homotopy theory) with equivalences representing each pointed type as the type of loops at the base point of the next type in the sequence. One also says that \(\K{G}{1+n}\) is the \emph{delooping} of \(\K{G}{n}\). It is an interesting task to characterize the category of \(n\)-types represented as  the \(k\)-fold delooping of some pointed type for all truncation levels~\(n\)) and \(k:\NN{}\). For example, Ulrik Buchholtz, Floris van Doorn and Egbert Rijke have proved in~\cite{HigherGroupsInHoTT} that the category of sets (0-types) represented as the \(k\)-fold delooping of another type is equivalent to the category of groups for \(k = 1\) and to the category of abelian groups for \(k \geq 2\). One direction of the equivalence constructs \(\K{G}{k}\), while the other direction takes the \(k\)-th homotopy group of the \(k\)-fold delooping. One step of the proof that these functors are inverse to each other consists of showing that for all (abelian) groups \(G\) and \(n:\NN{}\) the type \(\K{G}{n}\) is uniquely characterized by the corollary, i.e. that every pointed type which fulfills the properties listed in the corollary is equivalent to~\(\K{G}{n}\).

\begin{proof}[Proof of \Cref{thm:em-spectrum}]
For \(n \jeq 0\) the equivalence is given by \(\emloop{}\) as was proved in \Cref{prop:emloop-is-equiv}.
For \(n \jeq 1 + n'\) with \(n' : \NN{}\),
\[\begin{array}{cll}
& \K{G}{n} & \\
\jeq & \trunc{\iterSusp{n'}{\K{G}{1}}}{n} & \\
= & \trunc{\LoopSpace{\iterSusp{n}{\K{G}{1}}}}{n}
& \text{\(\heartsuit{}\) (see below)} \\
= & \LoopSpace{\trunc{\iterSusp{n}{\K{G}{1}}}{1+n}}
& \text{by~\cite[][Corollary 7.3.13]{HoTTBook}} \\
\jeq & \LoopSpace{\K{G}{1+n}}
\end{array}\]
For \(n \jeq 1\), \(\heartsuit{}\) follows from \Cref{thm:hstructure-suspension-pi2} and the equality \(\trunc{\K{G}{1}}{1} = \K{G}{1}\) (since \(\K{G}{1}\) already is a 1-type).
For \(n \geq 2\), the step \(\heartsuit{}\) is an instance of \Cref{cor:freudenthal-equivalence} since \(n \leq 2n'\) for such~\(n\). We also have to check that \(\iterSusp{n'}{\K{G}{1}}\) is \(n'\)-connected. This is true because \(\K{G}{1}\) is \(0\)-connected (\Cref{lem:kg1-0-connected}) and \(\iterSusp{m}{\blank}\) increases connectedness by~\(m\) for all~\(m:\NN{}\) (this follows by induction from~\cite[][Theorem 8.2.1]{HoTTBook}).
\end{proof}

\begin{proof}[Proof of \Cref{cor:em-properties}]
For \(n = 0\), \(\K{G}{n} \jeq G\) 0-truncated is by definition (a group is a \emph{set} with structure) and \((-1)\)-connected because it is inhabited (by \(e_G\)). The set of connected components is
\[\pi_0(\K{G}{0}) \jeq \trunc{G}{0} = G.\]

For \(n = 1\), \(\K{G}{n}\) is a 1-type by definition. We have already proved that \(\K{G}{1}\) is 0-connected (see \Cref{lem:kg1-0-connected}) and that there is an isomorphism \(\phi : G \to \pi_1(\K{G}{1})\) (see \Cref{thm:kg1-fundamental-group}).

For \(n \geq 2\), \(\K{G}{n}\) is \(n\)-truncated by definition and \((n-1)\)-connected as we have already seen in the proof of the theorem. The statement about homotopy groups follows by induction from \Cref{thm:em-spectrum} since taking the loop space shifts all homotopy groups by~1 (this follows from~\cite[][Corollary 7.3.13]{HoTTBook} and~\cite[][Lemma 7.3.15]{HoTTBook}).
\end{proof}

\section{Reduced cohomology}

\begin{definition}[{cf.~\cite[][Definition 5.1.3]{HomotopyGroupsOfSpheresInHoTT}}]
Let \(G\) be an abelian group and \(n : \NN{}\) and \(X\) a pointed type. The \emph{\(n\)-th reduced cohomology group of a pointed type~\(X\) with coefficients in~\(G\)} is
\[\redCoH{n}{X}{G} \deq \trunc{X \pto \K{G}{n}}{0}.\]
The group structure on \(\redCoH{n}{X}{G}\) is induced by the group structure on \(\LoopSpace{\K{1}{1+n}} \equiv \K{G}{n}\) given by path concatenation (and inversion). Since \(\K{G}{1+n} \jeq \LoopSpace{\K{G}{1+n}}\), the Eckmann--Hilton argument (cf.~\cite[][Theorem 2.1.6]{HoTTBook}) applies, from which it follows that \(\redCoH{n}{X}{G}\) is abelian.
For a type \(A\), not necessarily pointed, the \emph{\(n\)-th cohomology group of~\(A\)} is
\[\coH{n}{A}{G} \deq \redCoH{n}{\coproduct{A}{\unitType}}{G}\]
where we consider \(\coproduct{A}{\unitType}\), the coproduct (see~\cite[][Section 1.7]{HoTTBook}) of~\(A\) and the unit type~\(\unitType{}\), as a pointed type with base point~\(\Cinr(\unitTerm)\).
\end{definition}

\begin{remark}\label{rem:coh-definition-in-hott-agda}
In HoTT-Agda, \(\redCoH{n}{X}{G}\) is defined as \(\trunc{X \pto \LoopSpace{\K{G}{1+n}}}{0}\). This makes the definition of the group structure on \(\redCoH{n}{X}{G}\) easier. Our reason for not doing so here is that our definition makes defining the cup product in cohomology simpler since it doesn't require explicit translations between \(\K{G}{n}\) and \(\LoopSpace{\K{G}{1+n}}\).
\end{remark}

\begin{definition}
Each pointed map \(f : X \to Y\) between pointed types induces a homomorphism
\[\begin{array}{rcl}
f^* &:& \redCoH{n}{Y}{G} \to \redCoH{n}{X}{G} \\
&\deq& \truncFmap{\lam{s}s \circ f}{0}
\end{array}\]
on reduced cohomology for every abelian group \(G\) and \(n:\NN{}\).
\end{definition}

It is easy to prove that this mapping is contravariantly functorial in~\(f\).

On the Homotopy Type Theory Blog, Mike Shulman has sketched proofs \cite{CohomologyBlogPost} that these functors from the category of pointed types to the category of abelian groups form an ordinary reduced cohomology theory in the sense that they (type-theoretic analogue of the) Eilenberg--Steenrod axioms (except possibly the additivity axiom for arbitrary families). These proofs have been developed by multiple participants of the Special Year on Univalent Foundations of Mathematics at the IAS. Evan Cavallo has formalized them and written them up in more detail in chapter~3 of his master's thesis~\cite{SyntheticCohomologyInHoTT}. In chapter~4 he develops cohomology theory starting at the axioms by deducing consequences like the fact that additivity for finite wedges follows from the exactness axiom.
For finite cell complexes one can use cellular cohomology~\cite{CellularCohomologyInHoTT}, which is defined using the mapping degree of endomaps of spheres, to calculate the cohomology groups.

\begin{definition}
Each homomorphism \(\varphi : G \to H\) between abelian groups induces a homomorphism
\[\begin{array}{rcl}
\varphi_* &:& \redCoH{n}{X}{G} \to \redCoH{n}{X}{H} \\
&\deq& \truncFmap{\lam{s}\Kfmap{\varphi}{n} \circ s}{0}
\end{array}\]
on reduced cohomology for every pointed type~\(X\) and \(n:\NN{}\).
\end{definition}

\begin{lemma}
This mapping is (covariantly) functorial, that is
\begin{enumerate}
\item[(a)] \({(\idHom(G))}_* = \idfat{\redCoH{n}{X}{G}}\) for all abelian groups~\(G\) and
\item[(b)] \({(\psi \circ \varphi)}_* = \psi_* \circ \varphi_*\) for homomorphisms \(\varphi : G \to H\) and \(\psi : H \to K\) between abelian groups \(G\), \(H\) and~\(K\).
\end{enumerate}
\end{lemma}

\begin{proof}
Follows from \Cref{lem:em-functorial}.
\end{proof}