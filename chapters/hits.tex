\chapter{Some Higher Inductive Types}

To do interesting synthetic homotopy theory in type theory one needs to have a way to construct types that have some non-trivial higher path types. All types that we have seen so far, with the exception of the universe\footnote{One can use the univalence axiom to construct two nonequal paths between \(\boolType{}\) and \(\boolType{}\).}, are 0-types. This problem can be solved by adding some higher inductive types to the type theory. These are like ordinary inductive types in that they are defined by a list of constructors. The difference to ordinary inductive types is that they may also include constructors that produce paths instead of just points.

For example, the 1-sphere~\(\sphere{1}\) is be defined by the constructors
\begin{itemize}
\item \(\defname{base} : \sphere{1}\) and
\item \(\defname{loop} : \defname{base} = \defname{base}\).
\end{itemize}
Note that the second constructor does not give a term in~\(\sphere{1}\), but a path between two values in~\(\sphere{1}\).

Like for ordinary inductive type, there are induction and recursion principles for higher inductive types. One point worth mentioning is that the beta rules corresponding to the path constructors will only hold propositionally instead of judgementally.

\section{Truncation}

In classical homotopy theory, there is a construction that ``kills'' all homotopy groups of a topological space~\(X\) in degree \(n \in \mathbb{N}\) and higher but leaves all homotopy groups below untouched. It goes like this: To make the homotopy group \(\pi_n(X)\) vanish we add for all maps \(f : S^n \to X\) (where \(S^n\) is the \(n\)-spere) an \((n+1)\)-cell to \(X\), whose boundary is glued to \(X\) using \(f\). (One can show that it suffices to do this only for one function for each equivalence class of homotopy equivalent functions from \(S^n\) to~\(X\).) By cellular approximation this leaves all homotopy groups below degree \(n\) unchanged. Call the resulting space \(X'\). Now we kill \(\pi_{n+1}(X')\) by gluing in \((n+2)\)-cells resulting in a space~\(X''\) and keep going like this. In the union of the spaces \(X\), \(X'\), \(X''\), \ldots{} all homotopy at degree \(n\) and higher now vanish.

In homotopy type theory, there is a similar construction using a higher inductive type to kill all homotopical information above some truncation level \(n \geq -2\), which we describe in this section.

\begin{definition}[{\cite[][Section 7.3]{HoTTBook}}]
The \emph{\(n\)-truncation} of~\(A\) is the higher inductive 2-type \(\trunc{A}{n}\) defined by
\begin{itemize}
\item a function \(\truncProj{\blank}{n} : A \to \trunc{A}{n}\),
\item for each \(r : \sphere{n+1} \to \trunc{A}{n}\), a \emph{hub} point \(h(r) : \trunc{A}{n}\), and
\item for each \(r : \sphere{n+1} \to \trunc{A}{n}\) and each \(x : \sphere{n+1}\), a \emph{spoke} path \(s_r(x) : r(x) = h(r)\).
\end{itemize}
\end{definition}
If \(X\) is a pointed type with base point~\(x_0\), \(\trunc{X}{n}\) is also pointed with base point~\(\truncProj{x_0}{n}\).

The first constructor gives a projection function from \(A\) to \(\trunc{A}{n}\), while the last two functions serve to make every function \(r : \sphere{n+1} \to \trunc{A}{n}\) contractible to the constant function at \(h(r)\). One interesting difference to the construction described at the start of this section is that while the method from classical homotopy theory kills homotopy groups at ever higher degrees, this higher inductive type only kills them at level \(n+1\). But this turns out to be enough, since

\begin{proposition}
\(\trunc{A}{n}\) is an \(n\)-type.
\end{proposition}

\begin{proof}
The idea is to show that the type \(\iterSusp{n+1}((\trunc{A}{n}, b))\) is contractible for an choice of base point~\(b\). This iterated loop space is equivalent to the type
\[\sphere{n+1} \pto (\trunc{A}{n}, b).\]
To show that this type is contractible, we first show that for each pointed function \(f \jeq (r, p_f) : \sphere{n+1} \pto (\trunc{A}{n}, b)\), there is a homotopy
\[h : r \sim (\lam{x} b)\]
between the underlying function and the function that is constant at~\(b\). We construct this homotopy using the path constructors of \(\trunc{A}{n}\) by setting
\[h(x) \deq \concat{s_r(x)}{\concat{\inverse{s_r}(x_0)}{p_f}},\]
where \(x_0\) is the base point of \(\sphere{n+1}\). One can show that this homotopy induces a path between the pointed function~\(f\) and the pointed constant function \(((\lam{x} b), \refl{b})\), hence we have shown that \(\sphere{n+1} \pto (\trunc{A}{n}, b)\) is contractible.
See~\cite[][Lemma 7.3.1]{HoTTBook} for details.
\end{proof}

We will not state the recursion principle or induction principle that follows directly from the definition of~\(\trunc{A}{n}\), which has three cases, one for every constructor of \(\trunc{A}{n}\). Instead, we give simpler recursion and induction principles which describe how to eliminate \(\trunc{A}{n}\) into an \(n\)-type or a family of \(n\)-types.

The induction principle states that given a family \(P : \trunc{A}{n} \to \nUU{n}\) of \(n\)-types and a dependent function
\[g : \prod_{a:A} P(\truncProj{a}{n})\]
there is a dependent
\[f : \prod_{\wt{a} : \trunc{A}{n}} P(\wt{a})\]
with
\[f(\truncProj{a}{n}) \jeq g(a)\quad\text{for all \(a:A\).}\]
See~\cite[][Theorem 7.3.2]{HoTTBook} for a derivation of this induction principle.

The recursion principle of \(\trunc{A}{n}\) says that given an \(n\)-type \(B\) and a function \(g : A \to B\) there is an induced function \(f : \trunc{A}{n} \to B\) that satisfies the judgemental \(\beta{}\) rule \(f(\truncProj{a}{n}) \jeq g(a)\) for all~\(a:A\).

Using the recursion principle, one can show the following universal property of~\(\truncProj{A}{n}\):
One can show using the recursion principle that
For any \(n\)-type \(B\), the function
\[(\lam{g} g \circ \truncProj{\blank}{n})\quad:\quad(\trunc{A}{n} \to B) \equiv (A \to B)\]
is an equivalence (see~\cite[][Lemma 7.3.3]{HoTTBook}).

We may more generally call any \(n\)-type \(T\) together with a projection function \(t : A \to T\) that fullfills the above universal property the \(n\)-truncation of~\(A\), since the universal property guarantees that all types supporting it are equivalent. There are other methods for constructing such types. For example, Egbert Rijke showed~\cite{JoinConstruction} that \(n\)-truncations can be constructed in any univalent universe which is closed under homotopy coequalizers and possesses a natural numbers type.

\begin{definition}
For a map \(f : A \to B\) between types \(A\) and~\(B\) we define
\[\truncFmap{f}{n}\quad:\quad\trunc{A}{n} \to \trunc{B}{n}\]
using \(\truncProj{\blank}{n}\)-recursion by
\[\truncFmap{f}{n}[\truncProj{a}{n}] \deq \truncProj{f(a)}{n}\]
for \(a:A\).
If \(f \jeq (f', p_f) : X \pto Y\) is a pointed map, we can regard \(\truncFmap{f}{n}\) as a pointed map of type \(\trunc{X}{n} \pto \trunc{Y}{n}\) as witnessed by~\(p_{\truncFmap{f}{n}} \deq \ap{\truncProj{\blank}{n}}(p_f)\).
\end{definition}

% in HoTT-Agda: Trunc-fmap-idf
% in HoTT-Agda: ⊙Trunc-fmap-⊙idf
% in HoTT-Agda: Trunc-fmap-∘
% in HoTT-Agda: ⊙Trunc-fmap-⊙∘
\begin{lemma}\label{lem:trunc-fmap-functorial}
\begin{enumerate}
\item[(a)] For all pointed types \(X\) there is a path between pointed maps
\[\truncFmap{\idfat{X}}{n} = \idfat{\trunc{X}{n}.}\]
\item[(b)] For all pairs of pointed maps \(f : X \pto Y\) and \(g : Y \pto Z\) there is a path between pointed maps
\[\truncFmap{g \circ f}{n} = \truncFmap{g}{n} \circ \truncFmap{g}{n}.\]
\end{enumerate}
\end{lemma}

\begin{proof}
\begin{enumerate}
\item[(a)] We first construct a homotopy between the underlying unpointed maps. Let \(t:\trunc{X}{n}\). Our goal is to construct a path of type \(\truncFmap{\idfat{X}}{n}[t] = t\). Since this is an \(n{}\)-type (it is even an \(n-1\)-type if \(n \geq -1\)) we may assume by \(\truncProj{\blank}{n}\)-induction that \(t \jeq \truncProj{x}{n}\) for some \(x:X\). Now both sides reduce to \(\truncProj{x}{n}\), so we are done by \(\refl{\truncProj{x}{n}}\). By \Cref{rem:path-between-pointed-maps} we get a path between the pointed maps because \(p_{\truncFmap{\idfat{X}}{n}} \jeq \refl{\truncProj{x_0}{n}} \jeq \concat{\refl{\truncProj{x_0}{n}}}{p_{\idfat{\trunc{X}{n}}}}\).
\item[(b)] Let \(t:\trunc{X}{n}\). Since the goal type
\[\truncFmap{g \circ f}{n}[t] = \truncFmap{g}{n}[\truncFmap{f}{n}[t]]\]
is an \(n\)-type, we may assume \(t \jeq \truncProj{x}{n}\) for some \(x:X\). With \(t\) in this form, both sides reduce to \(\truncProj{g(f(x))}{n}\), so we are done by \(\refl{g(f(x))}\). Since
\[\begin{array}{cl}
& p_{\truncFmap{g \circ f}{n}} \\
\jeq & \ap{\truncProj{\blank}{n}}(\concat{\ap{g}(p_f)}{p_g}) \\
= & \concat{\ap{\truncProj{\blank}{n}}(\ap{g}(p_f))}{\ap{\truncProj{\blank}{n}}(p_g)} \\
= & \concat{\ap{\truncProj{\blank}{n} \circ g}(p_f)}{\ap{\truncProj{\blank}{n}}(p_g)} \\
= & \concat{\ap{\truncFmap{g}{n}}(\ap{\truncProj{\blank}{n}}(p_f))}{\ap{\truncProj{\blank}{n}}(p_g)} \\
\jeq & \concat{\refl{g(f(x_0))}}{p_{\truncFmap{g}{n} \circ \truncFmap{f}{n}}}
\end{array}\]
we get a path between pointed maps by \Cref{rem:path-between-pointed-maps}.\qedhere{}
\end{enumerate}
\end{proof}

\begin{definition}[{\cite[][Definition 7.5.1]{HoTTBook}}]
A type \(A\) is \(n\)-connected, for \(n \geq -2\), if and only if \(\trunc{A}{n}\) is contractible.
\end{definition}

Hence, an \(n\)-connected type is a type which, informally speaking, has nothing homotopically interesting going on in and below level~\(n\).

\begin{definition}[{\cite[][Definition 7.5.1]{HoTTBook}}]
A function \(f : A \to B\) is \(n\)-connected, for \(n \geq -2\), if and only if for all \(b:B\) the \emph{homotopy fiber}
\[\sum_{a:A} \parens{f(a) = b}\]
of~\(f\) over~\(b\) is \(n\)-connected.
\end{definition}

For more details on connectedness, see~\cite[][Section 7.5]{HoTTBook}.

\section{Suspension}

\begin{definition}
Let \(A\) be a type. The \emph{suspension of~\(A\)} is the higher inductive type \(\susp{A}\) defined by
\begin{itemize}
\item a point \(\suspNorth : \susp{A}\),
\item a point \(\suspSouth : \susp{A}\) and
\item for for each \(a : A\) a path \(\suspMerid(a) : \suspNorth{} = \suspSouth{}\).
\end{itemize}
The base point of \(\susp{A}\), when regarded as a pointed type, is \(\suspNorth{}\).
\end{definition}

One important property of the suspension is that for pointed types \(X\) and~\(Y\) there is an adjunction
\[\susp{X} \pto Y
\quad\jeq\quad
X \pto \LoopSpace{Y}\]
between the suspension and loop space functors just like there is in classical homotopy theory. We won't prove this here, but we will use one part of this adjunction throughout this thesis:

\begin{definition}
The unit map of the adjunction between suspension and loop space is
\[\eta : X \to \suspNorth = \suspNorth, \quad
x \mapsto \concat{\suspMerid(x)}{\inverse{\suspMerid(x_0)}}\]
where \(x_0\) is the base point of~\(X\).
\end{definition}

The recursion principle of \(\susp{A}\) states that given a type \(C\),
\begin{itemize}
\item a point \(n : C\),
\item a point \(s : C\) and
\item a family \(m : A \to n = s\) of paths between \(n\) and \(s\),
\end{itemize}
there is a function \(f : \susp{A} \to C\) with
\begin{itemize}
\item \(f(\suspNorth) \jeq n\),
\item \(f(\suspSouth) \jeq s\) and
\item \(\ap{f}(\suspMerid(a)) = m(a)\) for each \(a:A\).
\end{itemize}

The following is an application of the recursion principle:
Let \(f : A \to B\) be a map between types \(A\) and~\(B\). Then there is a map \(\susp{f} : \susp{A} \to \susp{B}\) with
\begin{itemize}
\item \(\suspFmap{f}[\suspNorth] \deq \suspNorth{}\),
\item \(\suspFmap{f}[\suspSouth] \deq \suspSouth{}\) and
\item \(\ap{\suspFmap{f}}(\suspMerid(a)) = \suspMerid(f(a))\) for each \(a:A\).
\end{itemize}
This map is pointed with witness \(\refl{\suspNorth}\).

The induction principle of \(\susp{A}\) says that given a family \(P : \susp{A} \to \UU{}\),
\begin{itemize}
\item a point \(n : P(\suspNorth)\),
\item a point \(s : P(\suspSouth)\) and
\item a family of dependent paths \(m : \prod_{a:A} \depid{P}{\suspMerid(a)}{n}{s}\),
\end{itemize}
there is a dependent function \(f : \prod_{s:\susp{A}} P(s)\) with
\begin{itemize}
\item \(f(\suspNorth) \jeq n\),
\item \(f(\suspSouth) \jeq s\) and
\item \(\apd{f}(\suspMerid(a)) = m(a)\) for each \(a:A\).
\end{itemize}

\begin{lemma}\label{lem:susp-functorial}
Suspension is functorial:
\begin{enumerate}
\item[(a)] \(\suspFmap{\idfat{A}} = \idfat{\susp{A}}\) as pointed maps for all types~\(A\),
\item[(b)] \(\suspFmap{g \circ f} = \suspFmap{g} \circ \suspFmap{f}\) as pointed maps for all maps \(f : A \to B\) and \(g : B \to C\).
\end{enumerate}
\end{lemma}

\begin{proof}
For each statement, we construct a homotopy \(h\) between the underlying maps by induction on~\(\susp{A}\).
\begin{enumerate}
\item[(a)] We construct for each \(s:\susp{A}\) a path \(h(s) : \suspFmap{\idfat{A}}[s] = s\). For \(s \jeq \suspNorth{}\) we set \(h(s) \deq \refl{\suspNorth}\) and for \(s \jeq \suspSouth{}\) we set \(h(s) \deq \refl{\suspSouth}\). The \(\suspMerid{}\) case requires us to construct for each \(a:A\) a dependent path
\[\depid{\lam{s} \suspFmap{\idfat{A}}[s] = s}{\suspMerid(a)}{\refl{\suspNorth}}{\refl{\suspSouth}}.\]
By \Cref{lem:dep-path-in-path-type} this type is equivalent to the type of commutative squares
\[\begin{tikzcd}[column sep={4cm,between origins}, row sep={2cm,between origins}]
\suspNorth
  \arrow[r, equals, "\ap{\suspFmap{\idfat{A}}}(\suspMerid(a))"]
  \arrow[d, equals, "\refl{\suspNorth}"'] &
\suspSouth
  \arrow[d, equals, "\refl{\suspSouth}"] \\
\suspNorth
  \arrow[r, equals, "\ap{\idfat{\susp{A}}}(\suspMerid(a))"] &
\suspSouth.
\end{tikzcd}\]
This diagram commutes because
\[\begin{array}{cl}
& \ap{\suspFmap{\idfat{A}}}(\suspMerid(a)) \\
& \reason{5cm}{by the \(\beta{}\) for \(\suspFmap{\idfat{A}}\)} \\
= & \suspMerid(a) \\
& \reason{5cm}{by~\cite[][Lemma 2.2.2 (iv)]{HoTTBook}} \\
= & \suspMerid(a).
\end{array}\]
\item[(b)] We construct for each \(s:\susp{A}\) a path \(h(s) : \suspFmap{g \circ f}[s] = \suspFmap{g}[\suspFmap{f}[s]]\). For \(s \jeq \suspNorth{}\) we set \(h(s) \deq \refl{\suspNorth}\) and for \(s \jeq \suspSouth{}\) we set \(h(s) \deq \refl{\suspSouth}\). The \(\suspMerid{}\) case requires us to construct for each \(a:A\) a dependent path
\[\depid{\lam{s} \suspFmap{g \circ f}[s] = \suspFmap{g}[\suspFmap{f}[s]]}{\suspMerid(a)}{\refl{\suspNorth}}{\refl{\suspSouth}}.\]
By \Cref{lem:dep-path-in-path-type} this type is equivalent to the type of commutative squares
\[\begin{tikzcd}[column sep={4cm,between origins}, row sep={2cm,between origins}]
\suspNorth
  \arrow[r, equals, "\ap{\suspFmap{g \circ f}}(\suspMerid(a))"]
  \arrow[d, equals, "\refl{\suspNorth}"'] &
\suspSouth
  \arrow[d, equals, "\refl{\suspSouth}"] \\
\suspNorth
  \arrow[r, equals, "\ap{\suspFmap{g} \circ \suspFmap{f}}(\suspMerid(a))"] &
\suspSouth.
\end{tikzcd}\]
This diagram commutes because
\[\begin{array}{cl}
& \ap{\suspFmap{g \circ f}}(\suspMerid(a)) \\
& \reason{5cm}{by the \(\beta{}\) for \(\suspFmap{g \circ f}\)} \\
= & \suspMerid(g(f(a))) \\
& \reason{5cm}{by the \(\beta{}\) for \(\suspFmap{g}\)} \\
= & \ap{\suspFmap{g}}(\suspMerid(f(a))) \\
& \reason{5cm}{by the \(\beta{}\) for \(\suspFmap{f}\)} \\
= & \ap{\suspFmap{g}}(\ap{\suspFmap{f}}(\suspMerid(a))).
\end{array}\]
\end{enumerate}
By \Cref{rem:path-between-pointed-maps} and the fact that \(h(\suspNorth)\) and each witness of base point preservation is the identity path, the statement also holds for pointed maps.
\end{proof}

\begin{corollary}\label{lem:transport-susp}
Let \(p : A = B\) be a path between types \(A\) and~\(B\). Then there is a path
\[\transport{\suspSymbol}{p} = \suspFmap{\coerce(p)}.\]
\end{corollary}

\begin{proof}
By path induction, we may assume that \(p\) is~\(\refl{A}\). The left-hand side now reduces to \(\idfat{\susp{A}}\) while the right-hand side reduces to \(\suspFmap{\idfat{A}}\). These are equal by \Cref{lem:susp-functorial}~(a).
\end{proof}

\begin{definition}
Let \(A\) be a type. For \(n : \NN{}\), the \emph{\(n\)-fold suspension of~\(A\)}, \(\iterSusp{n}{A}\), is defined by recursion by
\[\begin{array}{rcl}
\iterSusp{0}{A} &\deq& A, \\
\iterSusp{1+n}{A} &\deq& \susp{\iterSusp{n}{A}}.
\end{array}\]
\end{definition}
If \(X\) is a pointed type with base point~\(x_0\), then \(\iterSusp{n}{X}\) is pointed too, with base point \(\suspNorth{}\) if \(n \geq 1\) and \(x_0\) if \(n = 0\).

Similarly, we can use recursion to construct for each map \(f : A \to B\) and \(n : \NN{}\) a map \(\iterSuspFmap{n}{f} : \iterSusp{n}{A} \to \iterSusp{n}{B}\). If \(f : X \pto Y\) is a pointed map between pointed types, we get a pointed map \(\iterSuspFmap{n}{f} : \iterSusp{n}{X} \pto \iterSusp{n}{Y}\).

\begin{lemma}\label{lem:iter-susp-functorial}
Iterated suspension is functorial:
\begin{enumerate}
\item[(a)] \(\iterSuspFmap{m}{\idfat{X}} = \idfat{\iterSusp{m}{X}}\) as pointed maps for all types~\(X\) and \(m : \NN{}\),
\item[(b)] \(\iterSuspFmap{m}{g \circ f} = \iterSuspFmap{m}{g} \circ \iterSuspFmap{m}{f}\) as pointed maps for all pointed maps \(f : X \to Y\) and \(g : Y \to Z\).
\end{enumerate}
\end{lemma}

\begin{proof}
Follows from \Cref{lem:susp-functorial} by induction.
\end{proof}

\begin{corollary}\label{lem:transport-iter-susp}
Let \(p : A = B\) be a path between pointed types \(X\) and~\(Y\) and \(n:\NN{}\). Then there is a path between pointed maps
\[\transport{\iterSusp{n}{\blank}}{p} = \iterSuspFmap{n}{\coerce(p)}.\]
\end{corollary}

\begin{proof}
Similar to that of \Cref{lem:transport-susp}.
\end{proof}

\begin{definition}
Let \(A\) be a type. We define the family
\[\iterSuspPlus
\enspace:\enspace
\prod_{m,n:\NN}
\iterSusp{m}{\iterSusp{n}{A}} = \iterSusp{m+n}{A}\]
of paths between (pointed) types using induction by
\[\begin{array}{rcl}
\iterSuspPlus(0, n) &\deq& \refl{\iterSusp{n}{A}}, \\
\iterSuspPlus(1+m, n) &\deq& \ap{\suspSymbol}(\iterSuspPlus(m, n)).
\end{array}\]
\end{definition}

\begin{definition}
For \(m, n:\NN{}\) we let \(\iterSuspComm{}\) be the composition of paths
\[\begin{tikzcd}[column sep=2.4cm]
\iterSusp{m}{\iterSusp{n}{A}}
  \arrow[r, equal, "{\iterSuspPlus(m, n)}"] &
\iterSusp{m+n}{A}
  \arrow[r, equal, "{\ap{\lam{k} \iterSusp{k}{A}}(\plusComm(m, n))}"] &
\iterSusp{n+m}{A}
  \arrow[r, equal, "\inverse{\iterSuspPlus(n, m)}"] &
\iterSusp{n}{\iterSusp{m}{A}}.
\end{tikzcd}\]
\end{definition}

\begin{lemma}\label{lem:iter-susp-comm-diag}
For all pointed types \(X\) and \(m:\NN{}\) there is a path \(\iterSuspComm(m, m) = \refl{\iterSusp{m}{\iterSusp{m}{X}}}\).
\end{lemma}

\begin{proof}
Straightforward path algebra.
\end{proof}

\begin{lemma}\label{lem:iter-susp-comm-symm}
For all pointed types \(X\) and \(m,n:\NN{}\) there is a path \(\iterSuspComm(m, n) = \inverse{\iterSuspComm(n, m)}\).
\end{lemma}

\begin{proof}
Straightforward path algebra.
\end{proof}

% in HoTT-Agda: Susp^-+-0-r
% in HoTT-Agda: ⊙Susp^-+-0-r
\begin{lemma}\label{lem:iter-susp-plus-zero-r}
Let \(X\) be a pointed type and \(m : \NN{}\). Then
\[\concat{\iterSuspPlus(m, 0)}{\ap{\lam{k} \iterSusp{k}{X}}(\plusZeroR(m))} = \refl{\iterSusp{m}{X}}\]
where \(\plusZeroR(m) : m + 0 = m\).
\end{lemma}

\begin{proof}
By induction on~\(m\). For \(m \jeq 0\) we have \(\plusZeroR(0) = \refl{0}\) (since \(\NN{}\) is a set), hence both sides are equal to \(\refl{\iterSusp{0}{X}}\).
For \(m \jeq 1 + m'\) we have
\[\begin{array}[b]{cl}
& \concat{\iterSuspPlus(1+m', 0)}{\ap{\lam{k} \iterSusp{k}{X}}(\plusZeroR(1+m'))} \\
\jeq & \concat{\ap{\suspSymbol}(\iterSuspPlus(m', 0))}{\ap{\lam{k} \iterSusp{k}{X}}(\plusZeroR(1+m'))} \\
& \reason{8.5cm}{\(\plusZeroR(1+m') = \ap{\lam{n} 1+n}(\plusZeroR(m'))\) since \(\NN{}\) is a set} \\
= & \concat{\ap{\suspSymbol}(\iterSuspPlus(m', 0))}{\ap{\lam{k} \iterSusp{k}{X}}(\ap{\lam{n} 1+n}(\plusZeroR(m')))} \\
= & \concat{\ap{\suspSymbol}(\iterSuspPlus(m', 0))}{\ap{\lam{n} \iterSusp{1+n}{X}}(\plusZeroR(m'))} \\
= & \concat{\ap{\suspSymbol}(\iterSuspPlus(m', 0))}{\ap{\suspSymbol}(\ap{\lam{n} \iterSusp{n}{X}}(\plusZeroR(m')))} \\
= & \ap{\suspSymbol}(\concat{\iterSuspPlus(m', 0)}{\ap{\lam{n} \iterSusp{n}{X}}(\plusZeroR(m'))}) \\
= & \reason{5cm}{by the inductive hypothesis} \\
= & \ap{\suspSymbol}(\refl{\iterSusp{m'}{X}}) \\
\jeq & \refl{\iterSusp{1+m'}{X}}.
\end{array}\qedhere{}\]
\end{proof}

\begin{corollary}\label{lem:iter-susp-comm-zero}
Let \(X\) be a pointed type and \(m : \NN{}\). Then
\[\iterSuspComm(0, m) = \iterSuspComm(m, 0) = \refl{\iterSusp{m}{X}}.\]
\end{corollary}

\begin{proof}
Straightforward path algebra.
\end{proof}

% in HoTT-Agda: Susp^-+-assoc-coh
% in HoTT-Agda: ⊙Susp^-+-assoc-coh
\begin{lemma}\label{lem:iter-susp-plus-assoc}
Let \(X\) be a pointed type and \(m, n, o : \NN{}\). Let \(\plusAssoc(m, n, o) : (m+n)+o = m+(n+o)\). Then the following diagram of pointed types commutes:
\[\begin{tikzpicture}[commutative diagrams/every diagram]
\node (P0) at (90:2.3cm) {\(\iterSusp{m}{\iterSusp{n}{\iterSusp{o}{X}}}\)};
\node (P1) at (90+72:2cm) {\(\iterSusp{m+n}{\iterSusp{o}{X}}\)} ;
\node (P2) at (90+2*72:2cm) {\makebox[5ex][r]{\(\iterSusp{(m+n)+o}{X}\)}};
\node (P3) at (90+3*72:2cm) {\makebox[5ex][l]{\(\iterSusp{m+(n+o)}{X}\)}};
\node (P4) at (90+4*72:2cm) {\(\iterSusp{m}{\iterSusp{n+o}{X}}\)};
\path[commutative diagrams/.cd, every arrow, every label]
(P0) edge[commutative diagrams/equal] node[swap] {\(\iterSuspPlus(m, n)\)} (P1)
(P1) edge[commutative diagrams/equal] node[swap] {\(\iterSuspPlus(m+n, o)\)} (P2)
(P2) edge[commutative diagrams/equal] node[swap,yshift=-0.2cm] {\(\ap{\lam{k} \iterSusp{k}{X}}(\plusAssoc(m, n, o))\)} (P3)
(P4) edge[commutative diagrams/equal] node {\(\iterSuspPlus(m, n+o)\)} (P3)
(P0) edge[commutative diagrams/equal] node {\(\ap{\iterSusp{m}{\blank}}(\iterSuspPlus(n, o))\)} (P4);
\end{tikzpicture}\]
\end{lemma}

\begin{proof}
By induction over~\(m\).
For \(m \jeq 0\), the top left, the bottom right and the bottom path all reduce to the reflexivity path. Additionally, the top right path is equal to the bottom left path by~\cite[][Lemma 2.2.2 (iv)]{HoTTBook}. For \(m \jeq 1+m'\) we have a commutative diagram
\[\begin{tikzpicture}[commutative diagrams/every diagram]
\node (P0) at (90:3.8cm) {\(\iterSusp{1+m'}{\iterSusp{n}{\iterSusp{o}{X}}}\)};
\node (P1) at (90+72:3.5cm) {\(\iterSusp{(1+m')+n}{\iterSusp{o}{X}}\)} ;
\node (P2) at (90+2*72:3.5cm) {\makebox[5ex][r]{\(\iterSusp{((1+m')+n)+o}{X}\)}};
\node (P3) at (90+3*72:3.5cm) {\makebox[5ex][l]{\(\iterSusp{(1+m')+(n+o)}{X}\)}};
\node (P4) at (90+4*72:3.5cm) {\(\iterSusp{1+m'}{\iterSusp{n+o}{X}}\)};
\path[commutative diagrams/.cd, every arrow, every label]
(P0) edge[commutative diagrams/equal] node[swap] {\(\ap{\suspSymbol}(\iterSuspPlus(m', n))\)} (P1)
(P1) edge[commutative diagrams/equal] node[swap] {\(\ap{\suspSymbol}(\iterSuspPlus(m'+n, o))\)} (P2)
(P1) edge[commutative diagrams/phantom] node[yshift=-1.5cm] {\(\boxed{\begin{array}{c}\text{inductive hypothesis}\\(\text{and }\apFunctoriality)\end{array}}\)} (P4)
(P2) edge[commutative diagrams/equal, bend left=15] node[yshift=0cm] {\(\ap{\suspSymbol}(\ap{\lam{k} \iterSusp{k}{X}}(\plusAssoc(m', n, o)))\)} (P3)
(P2) edge[commutative diagrams/equal, bend right=15] node[swap,yshift=-0.1cm] {\(\ap{\lam{k} \iterSusp{k}{X}}(\plusAssoc(1+m', n, o))\)} (P3)
(P4) edge[commutative diagrams/equal] node {\(\ap{\suspSymbol}(\iterSuspPlus(m', n+o))\)} (P3)
(P0) edge[commutative diagrams/equal, bend left=15] node {\(\ap{\iterSusp{1+m'}{\blank}}(\iterSuspPlus(n, o))\)} (P4)
(P0) edge[commutative diagrams/equal, bend right=15] node[swap] {\(\ap{\suspSymbol}(\ap{\iterSusp{m'}{\blank}}(\iterSuspPlus(n,o)))\)} (P4);
\end{tikzpicture}\]
where the lens shaped cell in the top-right corner is filled by an instance of \(\apCirc{}\), and we used
\[\begin{array}{cl}
& \ap{\suspSymbol}(\ap{\lam{k} \iterSusp{k}{X}}(\plusAssoc(m', n, o))) \\
= & \ap{\lam{k} \iterSusp{1+k}{X}}(\plusAssoc(m', n, o)) \\
= & \ap{\lam{k} \iterSusp{k}{X}}(\ap{\lam{k} 1+k}(\plusAssoc(m', n, o))) \\
= & \ap{\lam{k} \iterSusp{k}{X}}(\plusAssoc(1+m', n, o))
\end{array}\]
to rewrite the bottom path.
\end{proof}

\begin{corollary}\label{cor:iter-susp-comm-succ}
Let \(X\) be a pointed type and \(m, n : \NN{}\).
Then the following diagrams commute:
\begin{enumerate}
\item[(a)] \(\begin{tikzcd}[column sep=3cm, row sep=1.1cm]
\iterSusp{m}{\iterSusp{1+n}{X}}
  \arrow[rr, equals, "{\iterSuspComm(m, 1+n)}"]
  \arrow[rd, equals, sloped, "{\iterSuspComm(m, 1)}", near start] &
&
\iterSusp{1+n}{\iterSusp{m}{X}} \\
&
\susp{\iterSusp{m}{\iterSusp{n}{X}}}
  \arrow[ru, equals, sloped, "{\ap{\suspSymbol}(\iterSuspComm(m, n))}", near end]
\end{tikzcd}\)
\item[(b)] \(\begin{tikzcd}[column sep=3cm, row sep=1.1cm]
\iterSusp{1+n}{\iterSusp{m}{X}}
  \arrow[rr, equals, "{\iterSuspComm(1+n, m)}"]
  \arrow[rd, equals, sloped, "{\ap{\suspSymbol}(\iterSuspComm(n, m))}", near start] &
&
\iterSusp{m}{\iterSusp{1+n}{X}} \\
&
\susp{\iterSusp{m}{\iterSusp{n}{X}}}
  \arrow[ru, equals, sloped, "{\iterSuspComm(1, m)}", near end]
\end{tikzcd}\)
\end{enumerate}
\end{corollary}

\begin{proof}
\begin{enumerate}
\item[(a)] Follows from \Cref{lem:iter-susp-plus-assoc} with \(o = 1\) using some path algebra.
\item[(b)] Follows from (a) using \Cref{lem:iter-susp-comm-symm}.\qedhere
\end{enumerate}
\end{proof}

\begin{lemma}[Naturality of \(\iterSuspPlus{}\) and \(\iterSuspComm{}\)]\label{lem:iter-susp-comm-natural}
For each pointed map \(f : X \pto Y\) and \(m, n:\NN{}\), the following squares commute:
\begin{enumerate}
\item[(a)] \(\begin{tikzcd}[column sep=3cm, row sep=1.5cm]
\iterSusp{m}{\iterSusp{n}{X}}
  \arrow[r, pointed, "\iterSuspFmap{m}{\iterSuspFmap{n}{f}}"]
  \arrow[d, equals, "{\iterSuspPlus(m, n)}"] &
\iterSusp{m}{\iterSusp{n}{Y}}
  \arrow[d, equals, "{\iterSuspPlus(m, n)}"] \\
\iterSusp{m+n}{X}
  \arrow[r, pointed, "\iterSuspFmap{m+n}{f}"] &
\iterSusp{m+n}{Y}
\end{tikzcd}\)
\item[(b)] \(\begin{tikzcd}[column sep=3cm, row sep=1.5cm]
\iterSusp{m}{\iterSusp{n}{X}}
  \arrow[r, pointed, "\iterSuspFmap{m}{\iterSuspFmap{n}{f}}"]
  \arrow[d, equals, "{\iterSuspComm(m, n)}"] &
\iterSusp{m}{\iterSusp{n}{Y}}
  \arrow[d, equals, "{\iterSuspComm(m, n)}"] \\
\iterSusp{n}{\iterSusp{m}{X}}
  \arrow[r, pointed, "\iterSuspFmap{n}{\iterSuspFmap{m}{f}}"] &
\iterSusp{n}{\iterSusp{m}{Y}}
\end{tikzcd}\)
\end{enumerate}
\end{lemma}

\begin{proof}
\begin{enumerate}
\item[(a)] By induction on~\(m\). The inductive step uses lemmas \ref{lem:susp-functorial} and~\ref{lem:transport-susp}.
\item[(b)] Follows from (a) and~\Cref{lem:pointed-transport-natural}.\qedhere
\end{enumerate}
\end{proof}

\section{Smash product}

\begin{definition}
Let \(X\) and \(Y\) be pointed types with basepoints \(x_0\) and \(y_0\) respectively.
The \emph{smash product} \(X \wedge Y\) is the higher inductive type defined by
\begin{itemize}
\item a function \(\smin : X \to Y \to X \wedge Y\),
\item points \(\smbasel : X \wedge Y\) and \(\smbaser : X \wedge Y\),
\item for every \(x : X\) a path \(\smgluel(x) : \smin(x, y_0) = \smbasel{}\) and
\item for every \(y : Y\) a path \(\smgluer(y) : \smin(x_0, y) = \smbaser{}\).
\end{itemize}
The base point of \(X \wedge Y\) is \(\smin(x_0, y_0)\).
\end{definition}

We can think of this type as the product \(X \times Y\) with all points \((x, y_0)\) identified with one additional and all points \((y, x_0)\) identified with another additional point. This type can also be constructed as the pushout of the diagram
\[\begin{tikzcd}
X \times Y &
\coproduct{X}{Y}
  \arrow[r]
  \arrow[l] &
\boolType
\end{tikzcd}\]
where \(\coproduct{X}{Y}\) is the coproduct of~\(X\) and~\(Y\), \(\boolType{}\) is the discrete type with inhabitants \(\Btrue{}\) and \(\Bfalse{}\), the left map is given by \(x \mapsto (x, y_0)\) on the summand~\(X\) and \(y \mapsto (x_0, y)\) on~\(Y\) and the right map is the constant \(\Btrue{}\) map on~\(X\) and the constant \(\Bfalse{}\) map on~\(Y\).

There is an equivalent definition of the smash product (which is used for example in~\cite{WedgeSumAndSmashProductInHoTT}) as the pushout of the diagram
\[\begin{tikzcd}
X \times Y &
X \vee Y
  \arrow[r]
  \arrow[l] &
\unitType
\end{tikzcd}\]
where \(X \vee Y\) is the \emph{wedge product} of~\(X\) and~\(Y\) which can be obtained from the coproduct \(\coproduct{X}{Y}\) by adding a path between \(x_0\) (in the left component) and \(y_0\) (in the right component). The left map sends each \(x\) in the left component to \((x, y_0)\) and each \(y\) in the right component to \((x_0, y)\). These two mappings are compatible for \(x \jeq x_0\) and \(y \jeq y_0\). This can be visualized as the product type \(X \times Y\) with an added base point and all points \((x, y_0)\) and all points \((x_0, y)\) identified with this base point. Since now there are two identifications of \((x_0, y_0)\) with the added base point, we need to require that there is a 2-path between them. In practice, the first definition is easier to work with since it doesn't contain any 2-path constructor at the minor drawback of having an additional point constructor.

The recursion principle of \(X \wedge Y\) states that, given a type \(C\) and
\begin{itemize}
\item a function \(i : X \to Y \to C\),
\item points \(\defname{bl}, \defname{br} : C\),
\item a family \(\defname{gl} : \prod_{x:X} i(x, y_0) = \defname{bl}\) of paths and
\item another family \(\defname{gr} : \prod_{y:Y} i(x_0, y) = \defname{br}\) of paths,
\end{itemize}
there is a function \(f : X \wedge Y \to C\) with
\begin{itemize}
\item \(f(\smin(x, y)) \jeq i(x, y)\) for all \(x : X\) and \(y : Y\),
\item \(f(\smbasel) \jeq \defname{bl}\) and \(f(\smbaser) \jeq \defname{br}\),
\item \(\ap{f}(\smgluel(x)) = \defname{gl}(x)\) for all \(x:X\) and
\item \(\ap{f}(\smgluer(y)) = \defname{gr}(y)\) for all \(y:Y\).
\end{itemize}

The recursion principle can be used for example to construct a map \(\smswap : X \wedge Y \to Y \wedge X\) with
\begin{itemize}
\item \(\smswap(\smin(x, y)) \jeq \smin(y, x)\) for all \(x:X\), \(y:Y\),
\item \(\smswap(\smgluel) \jeq \smswap(\smgluer) \jeq \smin(y_0, x_0)\),
\item \(\ap{\smswap}(\smgluel(x)) = \concat{\smgluer(x)}{\inverse{\smgluer(x_0)}}\) and
\item \(\ap{\smswap}(\smgluer(y)) = \concat{\smgluel(y)}{\inverse{\smgluel(y_0)}}\).
\end{itemize}
This map respects the base points of \(X \wedge Y\) and \(Y \wedge X\) as witnessed by the identity path. We can therefore regard \(\smswap{}\) as a pointed map of type \(X \wedge Y \pto Y \wedge X\).

Another application of the recursion principle is to define for pointed maps \(f \jeq (f', p) : X \pto X'\) and \(g = (g', q) : Y \pto Y'\) a map \(f \wedge g : X \wedge Y \to X' \wedge Y'\) with
\begin{itemize}
\item \((f \wedge g)(\smin(x, y)) \jeq \smin(f'(x), g'(y))\),
\item \((f \wedge g)(\smbasel) \jeq (f \wedge g)(\smbaser) \jeq \smin(x_0', y_0')\),
\item \(\ap{f \wedge g}(\smgluel(x)) = \concat{\ap{\smin(f(x), \blank)}(q)}{\concat{\smgluel(f(x))}{\inverse{\smgluel(x_0')}}}\) and
\item \(\ap{f \wedge g}(\smgluer(y)) = \concat{\ap{\smin(\blank, g'(y))}(p)}{\concat{\smgluer(g'(y))}{\inverse{\smgluer(y_0')}}}\).
\end{itemize}
The map \(f \wedge g\) can also be regarded as pointed with the path \(\apTwo{\smin}(p, q) : (f \wedge g)(\smin(x_0, y_0)) = \smin(x_0', y_0')\) as the witness of base point preservation.

The elimination principle of \(X \wedge Y\) states that, given a family of types \(P : X \wedge Y \to \UU{}\) and
\begin{itemize}
\item a dependent function \(i : \prod_{x:X} \prod_{y:Y} P(\smin(x,y))\),
\item points \(\defname{bl} : P(\smbasel)\) and \(\defname{br} : P(\smbaser)\),
\item a family \(\defname{gl} : \prod_{x:X} \depid{P}{\smbasel(x)}{i(x, y_0)}{\defname{bl}}\) of paths and
\item another family \(\defname{gr} : \prod_{y:Y} \depid{P}{\smbaser(y)}{i(x_0, y)}{\defname{br}}\) of paths,
\end{itemize}
there is a dependent function \(f : \prod_{s:X \wedge Y} P(s)\) with
\begin{itemize}
\item \(f(\smin(x, y)) \jeq i(x, y)\) for all \(x : X\) and \(y : Y\),
\item \(f(\smbasel) \jeq \defname{bl}\) and \(f(\smbaser) \jeq \defname{br}\),
\item \(\apd{f}(\smgluel(x)) = \defname{gl}(x)\) for all \(x:X\) and
\item \(\apd{f}(\smgluer(y)) = \defname{gr}(y)\) for all \(y:Y\).
\end{itemize}

This can be used to prove

\begin{lemma}\label{lem:swap-swap}
\(\smswap \circ \smswap = \idfat{X \wedge Y}\) as pointed maps
\end{lemma}

\begin{proof}[Proof sketch]
We first construct a path between the underlying maps by \(X \wedge Y\)-induction on \(P(s) \deq \smswap(\smswap(s)) = s\).
\begin{itemize}
\item For \(x:X\) and \(y:Y\) we have \(\refl{\smin(x,y)} : \smswap(\smswap(\smin(x,y))) = \smin(x,y)\).
\item We have \(\smgluel(x_0) : \smswap(\smswap(\smbasel)) = \smbasel{}\) and \(\smgluer(y_0) : \smswap(\smswap(\smbaser)) = \smbaser{}\).
\item For the \(\smgluel{}\)-case we have to construct a dependent path of type
\[\depid{\lam{s} \smswap(\smswap(s)) = s}{\smgluel(x)}{\refl{\smin(x,y_0)}}{\smgluel(x_0)},\]
which is equivalent to the simple path type
\[\concat{\ap{\smswap \circ \smswap}(\smgluel(x))}{\smgluel(x_0)} = \concat{\refl{\smin(x,y_0)}}{\smgluel(x)}.\]
A path of this type can be constructed by a straightforward, but tedious, calculation using the \(\beta{}\)-rules of~\(\smswap{}\) and~\cite[][Lemma 2.2.2]{HoTTBook}.
\item The \(\smgluer{}\)-case is similar.
\end{itemize}
All paths witnessing the preservation of basepoints as well as the \(\smin{}\)-case of the proof above are reflexivity, so we may conclude using reflexivity.
\end{proof}

Proofs about the smash product (like for example the preceding one) often involve exhibiting paths between points in the ``contractible component'' of the smash product, by which we mean all points of the form \((x, y_0)\), of the shape \((x_0, y)\), \(\smgluel{}\) and \(\smgluer{}\) as well as the paths \(\smgluel(x)\) for \(x:X\) and \(\smgluer(y)\) for \(y:Y\) as well as paths between such paths and sometimes even paths between those paths. This path algebra quickly gets very tedious. Guillaume Brunerie has formalized~\cite{GeneratedProofsForSmashProduct} this observation by giving a precise definition of ``contractible component'' and proving a meta-theorem (a theorem external to type theory) that states that all path types between points and paths in this ``contractible component'' are inhabited by a path which itself lies in this component. For this reason we will not give proofs for the next two lemmas. The interested reader can get all details from the mechanization.

\begin{lemma}
The smash product is functorial:
\begin{itemize}
\item \(\idfat{X} \wedge \idfat{Y} = \idfat{X \wedge Y}\) as pointed maps and
\item \((f_2 \wedge g_2) \circ (f_1 \wedge g_1) = (f_2 \circ f_1) \wedge (g_2 \circ g_1)\) as pointed maps for pointed maps \(f_1 : X \pto X'\), \(f_2 : X' \pto X''\), \(g_1 : Y \pto Y'\) and \(g_2 : Y' \pto Y''\).
\end{itemize}
\end{lemma}

\begin{lemma}[Naturality of \(\smswap{}\)]\label{lem:smash-swap-natural}
Let \(f : X \pto X'\) and \(g : Y \pto Y'\) be two pointed maps. Then the following diagram of pointed maps commutes:
\[\begin{tikzcd}[column sep=2.5cm, row sep=2cm]
X \wedge Y
  \arrow[r, pointed, "f \wedge g"]
  \arrow[d, pointed, "\smswap"'] &
X' \wedge Y'
  \arrow[d, pointed, "\smswap"] \\
Y \wedge X
  \arrow[r, pointed, "g \wedge f"] &
Y' \wedge X'
\end{tikzcd}\]
\end{lemma}

These lemmas are part of a proof that the smash product is a 1-coherent symmetric monoidal product on pointed types~\cite[][Definition 4.1.1]{HomotopyGroupsOfSpheresInHoTT}. It's particularly challenging to prove the coherence diagrams commutative. A direct approach using the induction principle of the smash product is very tedious for the technical reasons explained above, which is why Brunerie only gave a sketch proof. A detailed proof of this structure was later given by Floris van Doorn~\cite[][Section 4.3]{VanDoornThesis}. He uses a more indirect approach involving the adjunction
\[X \wedge Y \to Z
\quad\equiv\quad
X \pto \parens{Y \pto Z}\]
for all pointed types \(Z\) and Yoneda-style arguments.