\chapter{Generalized induction for \EilenbergMacLane spaces}

The cup product on cohomology will be induced by a cup product map on Eilenberg--MacLane spaces. In degrees \(m=n=1\) this is a map of type
\[\K{G}{1} \to \K{H}{1} \to \K{G \otimes H}{2}.\]
We would like to define this map using the recursion principle of \(\K{G}{1}\). However, there's one problem: We aren't allowed to use it since the target type, \(\K{H}{1} \to \K{G \otimes H}{2}\) is only a 2-type, not a 1-type! In this chapter we deal with this problem by redefining \(\K{G}{1}\) as a 2-HIT, which will naturally give us a recursion principle for defining maps into 2-types. Of course, we will then show that this new definition of~\(\K{G}{1}\) is equivalent to the old one, and in particular that the newly defined type actually is a 1-type.

From this point on, we adopt the following redefinition of \(\K{G}{1}\):
\begin{definition}
The \emph{\EilenbergMacLane{} space} \(\K{G}{1}\) is the higher inductive 2-type defined by
\begin{itemize}
\item \(\embase : \K{G}{1}\)
\item \(\emloop : G \to (\embase =_{\K{G}{1}} \embase)\)
\item \(\emloopComp : \prod_{g,h:G} \emloop(g \groupOp h) = \concat{\emloop(g)}{\emloop(h)} \)
\item \(\emloopCompCoh{}\), a constructor giving for every \(g_1, g_2, g_3 : G\) a 3-path that makes the diagram
\[\begin{tikzcd}[column sep=0.5cm, row sep=1cm]
% first line
&
\emloop((g_1 \groupOp g_2) \groupOp g_3)
  \arrow[rr, equal] &
&
\emloop(g_1 \groupOp (g_2 \groupOp g_3)) \\
% second line
\mathclap{\concat{\emloop(g_1 \groupOp g_2)}{\emloop(g_3)}}
  \arrow[ru, equal, start anchor={[xshift=2ex,yshift=1.5ex]}]
  \arrow[rd, equal, start anchor={[xshift=2ex,yshift=-1.5ex]}] &
&
\mathclap{\boxed{\emloopCompCoh(g_1, g_2, g_3)}} &
&
\mathclap{\concat{\emloop(g_1)}{\emloop(g_2 \groupOp g_3)}}
  \arrow[lu, equal, start anchor={[xshift=-2ex,yshift=1.5ex]}]
  \arrow[ld, equal, start anchor={[xshift=-2ex,yshift=-1.5ex]}] \\
% third line
&
\concat{(\concat{\emloop(g_1)}{\emloop(g_2)})}{\emloop(g_3)}
  \arrow[rr, equal] &
&
\concat{\emloop(g_1)}{(\concat{\emloop(g_2)}{\emloop(g_3)})}
\end{tikzcd}\]
where the top path is induced by associativity of \({\groupOp}\), the bottom path is associativity of path concatenation and all diagonal paths are (induced by) \(\emloopComp{}\) commute.
\end{itemize}
\end{definition}

The only differences of this definition compared to the earlier definition are ``2-type'' (instead of ``1-type'') in the first line and the new coherence 3-path constructor.

We refrain from stating the induction principle, since it involves a complicated dependent 3-path corresponding to \(\emloopCompCoh{}\). Luckily, we won't need it in its full generality. Instead we will use the induction principle only for families of 1-types, for which the type of dependent 3-paths becomes contractible. Hence the induction principle specialized to families of 1-types is identical to the induction principle for the definition of \(\K{G}{1}\) as a 1-HIT.

What we're after is the recursion principle of this generalized data type. It says that to define a function \(f : \K{G}{1} \to C\) to a 2-type \(C\) one must give
\begin{itemize}
\item a point \enspace{}\(c : C\),
\item a family of 1-paths \enspace{}\(\ell : G \to c = c\),
\item a family of 2-paths \enspace{}\(\ell\defname{\mhyphen{}comp} : \prod_{g,h:G} \ell(g \groupOp h) = \concat{\ell(x)}{\ell(y)}\), and
\item a function \(\ell\defname{\mhyphen{}comp\mhyphen{}coh}\), which given \(g_1, g_2, g_3 : G\) produces a 3-path making the diagram
\[\begin{tikzcd}[column sep=0.5cm, row sep=1cm]
% first line
&
\ell((g_1 \groupOp g_2) \groupOp g_3)
  \arrow[rr, equal] &
&
\ell(g_1 \groupOp (g_2 \groupOp g_3)) \\
% second line
\mathclap{\concat{\ell(g_1 \groupOp g_2)}{\ell(g_3)}}
  \arrow[ru, equal, start anchor={[xshift=1ex,yshift=1.5ex]}]
  \arrow[rd, equal, start anchor={[xshift=1ex,yshift=-1.5ex]}] &
&
\mathclap{\boxed{\ell\defname{\mhyphen{}comp\mhyphen{}coh}(g_1, g_2, g_3)}} &
&
\mathclap{\concat{\ell(g_1)}{\ell(g_2 \groupOp g_3)}}
  \arrow[lu, equal, start anchor={[xshift=-1ex,yshift=1.5ex]}]
  \arrow[ld, equal, start anchor={[xshift=-1ex,yshift=-1.5ex]}] \\
% third line
&
\concat{(\concat{\ell(g_1)}{\ell(g_2)})}{\ell(g_3)}
  \arrow[rr, equal] &
&
\concat{\ell(g_1)}{(\concat{\ell(g_2)}{\ell(g_3)})}
\end{tikzcd}\]
where the top path is induced by associativity of \({\groupOp}\), the bottom path is associativity of path concatenation and the diagonal paths are (induced by) \(\ell\defname{\mhyphen{}comp}\) commute.
\end{itemize}
The resulting function \(f\) satisfies:
\begin{itemize}
\item \(f(\embase) \jeq c\) holds.
\item There is a path \(\emloopBeta(g) : \ap{f}(\emloop(g)) = \ell(g)\) for all \(g : G\).
\item For all \(g, h : G\), there is a path \(\emloopCompBeta(g, h)\), which makes the following diagram commute:
\[\begin{tikzcd}[column sep=4.5cm]
\ap{f}(\emloop(g \groupOp h))
  \arrow[rdd, phantom, "\boxed{\emloopCompBeta(g, h)}"]
  \arrow[r, equal, "{\emloopBeta(g \groupOp h)}"]
  \arrow[d, equal, "{\ap{\ap{f}}(\emloopComp{(g, h))}}"'{xshift=-0.1cm}] &
\ell(g \groupOp h)
  \arrow[dd, equal, "{\ell\defname{\mhyphen{}comp\mhyphen{}coh}(g, h)}"{xshift=0.1cm}] \\
\ap{f}(\concat{\emloop(g)}{\emloop(h)})
  \arrow[d, equal, "{\apFunctoriality[\ap{f}][\emloop(g), \emloop(h)]}"'{xshift=-0.1cm}] \\
\concat{\ap{f}(\emloop(g))}{\ap{f}(\emloop(h))}
  \arrow[r, equal, "{\apTwo{\concat{\blank}{\blank}}(\emloopBeta(g), \emloopBeta(h))}"] &
\concat{\ell(g)}{\ell(h)}
\end{tikzcd}\]
\end{itemize}

We now want to show that even though \(\K{G}{1}\) is now defined as a 2-HIT, it is still a 1-type. We do this by repairing the proof of \Cref{prop:emloop-is-equiv}, which states that \(\emloop : G \to \embase = \embase{}\) is an equivalence. It follows that \(\embase = \embase{}\) is a set. By \(\K{G}{1}\)-induction (for families of mere propositions -- hence giving all required paths is trivial and we have to check only the \(\embase\) case) on both \(x\) and \(y\) we get that \(x = y\) is a set for all \(x, y : \K{G}{1}\), which is the definition of \(\K{G}{1}\) being a 1-type.

The previous proof \Cref{prop:emloop-is-equiv} uses the fact that (according to the definition as a 1-HIT) \(\K{G}{1}\) is a 1-type once: In the definition of \(\decode : \prod_{z:\K{G}{1}} \Codes(z) \to \embase = z\) to provide the required dependent 2-paths. Our job is to define \(\decode\) again without using this fact. We first prove a helper lemma for defining fiberwise transformations such as \(\decode\).

\begin{lemma}
To define a fiberwise transformation \(f : \prod_{z:\K{G}{1}} C(z) \to D(z)\) for two fibrations \(C, D : \K{G}{1} \to \UU\) where \(D(\embase)\) is a 1-type it suffices to specify
\begin{itemize}
\item a map \enspace{}\(b : C(\embase) \to D(\embase)\),
\item a family of 1-paths \enspace{}\(\ell : \prod_{g:G} \prod_{c:C(\embase)} \transp{D}{\emloop(g)}{b(c)} = b(\transp{C}{\emloop(g)}{c})\), and
\item for every pair \(g_1, g_2 : G\) and \(c:C(\embase)\) a 2-path making the following diagram commute:
\[\begin{tikzcd}[column sep=0.5cm, row sep=1cm]
% 0th row
&
\mathclap{\transp{D}{\concat{\emloop(g_1)}{\emloop(g_2)}}{b(c)}}
  \arrow[ld, equal, start anchor={[xshift=-4ex,yshift=-2ex]}, near end, "\inverse{\ap{\lam{p} \transp{D}{p}{b(c)}}(\emloopComp(g_1, g_2))}"']
  \arrow[rd, equal, start anchor={[xshift=+4ex,yshift=-2ex]}] \\
% 1st row
\transp{D}{\emloop(g_1 \groupOp g_2)}{b(c)}
  \arrow[dd, equal, "{\ell(g_1 \groupOp g_2, c)}"'] &
&
\transp{D}{\emloop(g_2)}{\transp{D}{\emloop(g_1)}{b(c)}}
  \arrow[d, equal, "{\ap{\transp{D}{\emloop(g_2)}{\blank}}(\ell(g_1, c))}"] \\
% 2nd row
&
&
\transp{D}{\emloop(g_2)}{b(\transp{C}{\emloop(g_1)}{c})}
  \arrow[d, equal, "{\ell(g_1, \transp{C}{\emloop(g_1)}{c})}"] \\
% 3rd row
b(\transp{C}{\emloop(g_1 \groupOp g_2)}{c}) &
&
b(\transp{C}{\emloop(g_2)}{\transp{C}{\emloop(g_1)}{c}}) \\
% 4th row
&
\mathclap{b(\transp{C}{\concat{\emloop(g_1)}{\emloop(g_2)}}{c})}
  \arrow[lu, equal, start anchor={[xshift=-4ex,yshift=2ex]}, near end, "{\ap{\lam{p} b(\transp{C}{p}{c})}(\emloopComp(g_1, g_2))}"]
  \arrow[ru, equal, start anchor={[xshift=+4ex,yshift=1.25ex]}]
\end{tikzcd}\]
\end{itemize}
The constructed fiberwise transformation \(f\) satisfies:
\begin{itemize}
\item \(f(\embase) \jeq b\)
\item \(\apd{f}(\emloop(g))\) corresponds under the equivalence from~\Cref{lem:dep-path-in-arrow-type} to \(\ell(g)\).
\end{itemize}
\end{lemma}

\begin{proof}
First, we introduce some notation: For any term \(q : f_0 \trRel{p} f_1\) let \(\overline{q}\) be the dependent path corresponding to \(q\) under the equivalence from~\Cref{lem:dep-path-in-arrow-type}.
We use the induction principle of \(\K{G}{1}\) for the family \(F \deq \lam{z} C(z) \to D(z)\) of 1-types (see~\Cref{rem:kg1-induction-principle-1-type}) with the following data:
\begin{itemize}
\item As the image of the basepoint we chose \(b\).
\item We now have to give for all \(g:G\) a dependent path \(\depid{F}{\emloop(g)}{b}{b}\). For this we use \(\overline{\funext(\ell(g))}\).
\item Finally, we have to construct for all \(g_1,g_2:G\) a dependent path of type
\[\depid{\lam{p} (\depid{F}{p}{b}{b})}{\emloopComp(g_1, g_2)}{\overline{\funext(\ell(g_1 \groupOp g_2))}}{\depConcat{\overline{\funext(\ell(g_1))}}{\overline{\funext(\ell(g_2))}}}. \]
We use lemma~\Cref{lem:dep-path-in-arrow-type-composition} to rewrite the right-hand side:
\[\depid{\lam{p} (\depid{F}{p}{b}{b})}{\emloopComp(g_1, g_2)}{\overline{\funext(\ell(g_1 \groupOp g_2))}}{\overline{\funext(\treComp(\ell(g_1), \ell(g_2)))}}\]
This is the image of a dependent path of type
\[\depid{\lam{p} b \trRel{p} b}{\emloopComp(g_1, g_2)}{\funext(\ell(g_1 \groupOp g_2))}{\funext(\treComp(\ell(g_1), \ell(g_2)))}\]
under the function \(\apDown{\lam{x} \overline{x}}\). By \Cref{lem:dep-path-in-path-type} about dependent paths in path types this is equivalent to
\[\begin{array}{c l}
& \concat{\funext(\ell(g_1 \groupOp g_2))}{\ap{\lam{p} b \fcomp \transport{C}{p}}(\emloopComp(g_1, g_2))} \\
= & \concat{\ap{\lam{p} \transport{D}{p} \fcomp b}(\emloopComp(g_1, g_2))}{\funext(\treComp(\ell(g_1), \ell(g_2)))}.
\end{array}\]
Using function extensionality and its properties, this is in turn equivalent to
\[\begin{array}{c l}
& \concat{\ell(g_1 \groupOp g_2, c)}{\ap{\lam{p} b(\transport{C}{p}[c])}(\emloopComp(g_1, g_2))} \\
= & \concat{\ap{\lam{p} \transport{D}{p}[b(c)]}(\emloopComp(g_1, g_2))}{\treComp(\ell(g_1), \ell(g_2), c)}
\end{array}\]
holding for all \(c:C(\embase)\). This last path type expresses the commutativity of the diagram (notice that \(\treComp(\ell(g_1), \ell(g_2), c)\) is exactly the right side of the diagram). \qedhere
\end{itemize}
\end{proof}

\begin{proof}[Patch for the proof of \Cref{prop:emloop-is-equiv}]
We use the preceding lemma to define \(\decode : \prod_{z:\K{G}{1}} \Codes(z) \to \embase = z\). For \(b\) we use \(\emloop : \Codes(\embase) \to \embase = \embase\). For \(\ell\) we use the composite path
\[
\ell(g, h) \deq
\mleft\{\quad\begin{array}{c l}
& \transport{\lam{z} \embase = z}{\emloop(g)}[\emloop(h)] \\
& \reason{5cm}{\(\covTransport(\emloop(h), \emloop(g))\)} \\
= & \concat{\emloop(h)}{\emloop(g)} \\
& \reason{5cm}{\(\inverse{\emloopComp(h, g)}\)} \\
= & \emloop(h \groupOp g) \\
& \reason{5cm}{\(\ap{\emloop}(\inverse{\transportCodes(g, h)})\)} \\
= & \emloop(\transport{\Codes}{\emloop(g)}[h])
\end{array}
\mright.\]
that already appeared in the definition of \(\decode{}\) from the previous proof.
The required commutativity proof is shown in \Cref{fig:emloop-is-equiv-patch}.
The rest of the proof of~\Cref{prop:emloop-is-equiv} goes through without problems.
\end{proof}

\begin{landscape}
\begin{figure}
\[\begin{tikzcd}[column sep=0.4cm, row sep=2cm]
% 1st row
\transp{z. \embase = z}{\emloop(g_1 \groupOp g_2)}{\emloop(h)}
  \arrow[r, equal]
  \arrow[d, equal]
  \arrow[rd, phantom, "\boxed{\hoNat}"]
  \arrow[ddd, equal, bend right=50, ""{name=C}] &
\transp{z. \embase = z}{\concat{\emloop(g_1)}{\emloop(g_2)}}{\emloop(h)}
  \arrow[rr, equal]
  \arrow[d, equal]
  \arrow[rrd, phantom, "\boxed{\covTransportCoh(\emloop(h), \emloop(g_1), \emloop(g_2))}"] &
&
\transp{z. \embase = z}{\emloop(g_2)}{\transp{z. \embase = z}{\emloop(g_1)}{\emloop(h)}}
  \arrow[d, equal]
  \arrow[ddd, equal, bend left=70, ""{name=A}] \\
% 2nd row
\concat{\emloop(h)}{\emloop(g_1 \groupOp g_2)}
  \arrow[r, equal]
  \arrow[d, equal, ""{name=D}]
  \arrow[phantom, from=C, to=D, "\jeq"] &
\concat{\emloop(h)}{(\concat{\emloop(g_1)}{\emloop(g_2)})}
  \arrow[r, equal]
  \arrow[d, phantom, "\boxed{\emloopCompCoh(h, g_1, g_2)}"] &
\concat{(\concat{\emloop(h)}{\emloop(g_1)})}{\emloop(g_2)}
  \arrow[r, equal]
  \arrow[d, equal]
  \arrow[rd, phantom, "\boxed{\hoNat}"] &
\transp{z. \embase = z}{\emloop(g_2)}{\concat{\emloop(h)}{\emloop(g_1)}}
  \arrow[d, equal, ""{name=B}]
  \arrow[phantom, from=A, to=B, "\boxed{\apFunctoriality}"] \\
% 3rd row
\emloop(h \groupOp (g_1 \groupOp g_2))
  \arrow[r, equal]
  \arrow[d, equal]
  \arrow[rdd, phantom, "\circledStar"] &
\emloop((h \groupOp g_1) \groupOp g_2)
  \arrow[r, equal]
  \arrow[d, equal]
  \arrow[rd, phantom, "\boxed{\hoNat}"] &
\concat{\emloop(h \groupOp g_1)}{\emloop(g_2)}
  \arrow[r, equal]
  \arrow[d, equal]
  \arrow[rd, phantom, "\boxed{\hoNat}"] &
\transp{z. \embase = z}{\emloop(g_2)}{\emloop(h \groupOp g_1)}
  \arrow[d, equal] \\
% 4th row
\emloop(\transp{\Codes}{\emloop(g_1 \groupOp g_2)}{h})
  \arrow[d, equal] &
\emloop(\concat{\transp{\Codes}{\emloop(g_1)}{h}}{g_2})
  \arrow[r, equal]
  \arrow[d, equal] &
\concat{\emloop(\transp{\Codes}{\emloop(g_1)}{h})}{\emloop(g_2)}
  \arrow[r, equal]
  \arrow[ld, phantom, bend left=20, near start, "\jeq"] &
\transp{z. \embase = z}{\emloop(g_2)}{\emloop(\transp{\Codes}{\emloop(g_1)}{h})}
  \arrow[lld, equal, bend left=10] \\
% 5th row
\emloop(\transp{\Codes}{\concat{\emloop(g_1)}{\emloop(g_2)}}{h})
  \arrow[r, equal] &
\emloop(\transp{\Codes}{\emloop(g_2)}{\transp{\Codes}{\emloop(g_1)}{h}})
\end{tikzcd}\]
\vspace{0.5cm}
\centering\(\circledStar{}\) \, commutes since it is the image of a diagram in~\(G\) (which commutes since \(G\) is a set) under \(\emloop\).
\caption{The commutative diagram used to patch \Cref{prop:emloop-is-equiv}.}
\label{fig:emloop-is-equiv-patch}
\end{figure}
\end{landscape}