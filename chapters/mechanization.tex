\chapter{Mechanization}

All results in this thesis have been mechanized in HoTT-Agda~\cite{HoTTAgda}, a library of Agda code containing basic lemmas and definitions as well as proofs of non-trivial, interesting theorems in Homotopy Type Theory\footnote{One half of my mechanization has already been merged in the master branch of the HoTT-Agda repository. The second half is contained in the pull request at address \url{https://github.com/HoTT/HoTT-Agda/pull/47} hasn't yet been merged into the main branch at the time of writing.}. Summing all lines of code added, and subtracting lines of code deleted, there are now 16538 more lines of code in the library because of my efforts.

Kuen-Bang Hou has already written about the differences between the type theory of Agda and HoTT and shared practical advice on the mechanization of theorems in Agda in their thesis~\cite[][Chapter 4]{FavoniasThesis}. Since I find myself agreeing with them on all points and do not want to repeat what they have written, I am going to add my own advice that I gained through the experience of mechanization.

One piece of advice is: Building machinery pays off. In this thesis there are a lot of commutative diagrams, both of paths and of (pointed) maps. When formalised, these commutative diagrams turn into rather long chains of equations.
For example, consider the following diagram of paths:

\[\begin{tikzcd}[column sep={2cm,between origins}, row sep={2cm,between origins}]
a_{00}
  \arrow[rr, equals, bend left=20, "\concat{p_{00}}{p_{10}}"]
  \arrow[r, equals, "p_{01}"']
  \arrow[d, equals, "q_0"] &
a_{10}
  \arrow[r, equals, "p_{10}"']
  \arrow[d, equals, "q_1"] &
a_{20}
  \arrow[d, equals, "q_2"] \\
a_{01}
  \arrow[rr, equals, bend right=20, "\concat{p_{01}}{p_{11}}"]
  \arrow[r, equals, "p_{01}"] &
a_{11}
  \arrow[r, equals, "p_{11}"'] &
a_{21}
\end{tikzcd}\]

Assume that there is a proof of commutativity of the left diagram in the form of a path
\[sq_0 : \concat{q_0}{p_{01}} = \concat{p_{00}}{q_1}\]
and a proof of commutativity of the right diagram in the form of a path
\[sq_1 : \concat{q_1}{p_{11}} = \concat{p_{10}}{q_2}.\]
Then the following calculation in Agda shows that the outer diagram commutes:

% module _ {i} {A : Type i} where
%
%   commutative-diagram :
%     {a₀₀ a₁₀ a₂₀ a₀₁ a₁₁ a₂₁ : A}
%     (q₀ : a₀₀ == a₀₁)
%     (q₁ : a₁₀ == a₁₁)
%     (q₂ : a₂₀ == a₂₁)
%     (p₀₀ : a₀₀ == a₁₀) (p₁₀ : a₁₀ == a₂₀)
%     (p₀₁ : a₀₁ == a₁₁) (p₁₁ : a₁₁ == a₂₁)
%     → q₀ ∙ p₀₁ == p₀₀ ∙ q₁
%     → q₁ ∙ p₁₁ == p₁₀ ∙ q₂
%     → q₀ ∙ (p₀₁ ∙ p₁₁) == (p₀₀ ∙ p₁₀) ∙ q₂
%   commutative-diagram q₀ q₁ q₂ p₀₀ p₁₀ p₀₁ p₁₁ sq₀ sq₁ =
\begin{code}
q₀ ∙ (p₀₁ ∙ p₁₁)
  =⟨ ! (∙-assoc q₀ p₀₁ p₁₁) ⟩
(q₀ ∙ p₀₁) ∙ p₁₁
  =⟨ ap (_∙ p₁₁) sq₀ ⟩
(p₀₀ ∙ q₁) ∙ p₁₁
  =⟨ ∙-assoc p₀₀ q₁ p₁₁ ⟩
p₀₀ ∙ (q₁ ∙ p₁₁)
  =⟨ ap (p₀₀ ∙_) sq₁ ⟩
p₀₀ ∙ (p₁₀ ∙ q₂)
  =⟨ ! (∙-assoc p₀₀ p₁₀ q₂) ⟩
(p₀₀ ∙ p₁₀) ∙ q₂ =∎
\end{code}

One good thing about this is that this calculation is readable, thanks to Agda's very flexible notation allowing the definition of a DSL for chains of equations. But there are two problems: First, when rewriting a subexpression (as done in the second and third steps) one needs to repeat the whole expression with a variable as a placeholder in a call to \verb+ap+. This is annoying. Secondly, and more seriously, three of the five steps explicitly invoke associativity of path concatenation. When proving more complicated diagrams, it would not be unusual to apply associativity several times in a row, just to get the term in the right form where one can apply an equality to rewrite a subterm. This is not acceptable! One should not have to think about associativity, just like one doesn't think about it when drawing commutative diagrams!

To solve these problems, I introduced some machinery. First, I defined the data type of sequences of paths as follows:
\begin{code}
module _ {i} {A : Type i} where
  infixr 80 _◃∙_
  data PathSeq : A → A → Type i where
    [] : {a : A} → PathSeq a a
    _◃∙_ : {a a' a'' : A} (p : a == a') (s : PathSeq a' a'') → PathSeq a a''

  infix 30 _=-=_
  _=-=_ = PathSeq

  infix 90 _◃∎
  _◃∎ : {a a' : A} → a == a' → a =-= a'
  _◃∎ {a} {a'} p = p ◃∙ []
\end{code}

In our example, \verb+q₀ ◃∙ p₀₁ ◃∙ p₁₁ ◃∎+ is such a sequence of paths.
We can deal with these sequences a lot like we can deal with lists: We can concatenate them, we can split them at positions indicated by integers and we can reverse them (also reversing each constituent path in the process). The following function ``realizes'' a path sequence, producing an ordinary path between the start and the end, by concatenating all constituent paths in a right-associative manner:

\begin{code}
  ↯ : {a a' : A} (s : a =-= a') → a == a'
  ↯ [] = idp
  ↯ (p ◃∙ []) = p
  ↯ (p ◃∙ s@(_ ◃∙ _)) = p ∙ ↯ s
\end{code}

For our example, this produces the normal path \verb+q₀ ∙ p₀₁ ∙ p₁₁+. The following datatype defines a relation on path sequences:

\begin{code}
  record _=ₛ_ {a a' : A} (s t : a =-= a') : Type i where
    constructor =ₛ-in
    field
      =ₛ-out : ↯ s == ↯ t
\end{code}

In words, two path sequences are related by \verb+=ₛ+ if (and only if) there is a path between their realizations. In our commutative square example, the assumptions that the left and the right each commute can be expressed as the existence of values

\begin{code}
sq₀ : q₀ ◃∙ p₀₁ ◃∎ =ₛ p₀₀ ◃∙ q₁ ◃∎
sq₁ : q₁ ◃∙ p₁₁ ◃∎ =ₛ p₁₀ ◃∙ q₂ ◃∎
\end{code}

Using these values, we can prove the commutativity of the composition of the squares by

% commutative-diagram' :
%   {a₀₀ a₁₀ a₂₀ a₀₁ a₁₁ a₂₁ : A}
%   (q₀ : a₀₀ == a₀₁)
%   (q₁ : a₁₀ == a₁₁)
%   (q₂ : a₂₀ == a₂₁)
%   (p₀₀ : a₀₀ == a₁₀) (p₁₀ : a₁₀ == a₂₀)
%   (p₀₁ : a₀₁ == a₁₁) (p₁₁ : a₁₁ == a₂₁)
%   → q₀ ◃∙ p₀₁ ◃∎ =ₛ p₀₀ ◃∙ q₁ ◃∎
%   → q₁ ◃∙ p₁₁ ◃∎ =ₛ p₁₀ ◃∙ q₂ ◃∎
%   → q₀ ◃∙ p₀₁ ◃∙ p₁₁ ◃∎ =ₛ p₀₀ ◃∙ p₁₀ ◃∙ q₂ ◃∎
\begin{code}
q₀ ◃∙ p₀₁ ◃∙ p₁₁ ◃∎
  =ₛ⟨ 0 & 2 & sq₀ ⟩ -- rewrite m=2 segments starting at position n=0 using sq₀
p₀₀ ◃∙ q₁ ◃∙ p₁₁ ◃∎
  =ₛ⟨ 1 & 2 & sq₁ ⟩ -- rewrite m=2 segments starting at position n=1 using sq₁
p₀₀ ◃∙ p₁₀ ◃∙ q₂ ◃∎ ∎ₛ
\end{code}

This code uses a function
\begin{code}
module _ {i} {A : Type i} {a a' : A} where

  infixr 10 _=ₛ⟨_&_&_⟩_
  _=ₛ⟨_&_&_⟩_ : (s : a =-= a') {u : a =-= a'}
    → (m n : ℕ)
    → {r : point-from-start m s =-= point-from-start n (drop m s)}
    → take n (drop m s) =ₛ r
    → take m s ∙∙ r ∙∙ drop n (drop m s) =ₛ u
    → s =ₛ u
\end{code}
which forms the core of our machinery to deal with complicated diagrams. This function is used to rewrite a subsequence of a path sequence (which is given by the first argument \verb+s+), which starts at a position specified by a natural number \verb+m+ (with \(0\) being the start) and contains the next \verb+n+ segments from this position. The next argument \verb+r+ contains the sequence that replaces the subsequence and the argument after that contains a proof that the subsequence is related to~\verb+r+ by \verb+=ₛ+. The last argument of the function contains the rest of the calculation. This makes it possible to write such clean-looking chains of equations as above.

Therefore, the subsequence that should be rewritten can be indicated by a pair of natural numbers and associativity is being dealed with ``behind the scenes''.

For a final example consider the function \(\apFunctoriality{}\) (which was defined in \Cref{lem:ap-functoriality}). The type of this function involves the application of \(\ap{f}\) to each segment in a sequence of paths. This can be defined in Agda as a recursive function:
\begin{code}
module _ {i j} {A : Type i} {B : Type j} (f : A → B) where

  ap-seq : {a a' : A} → a =-= a' → f a =-= f a'
  ap-seq [] = []
  ap-seq (p ◃∙ s) = ap f p ◃∙ ap-seq s
\end{code}

The definition of \(\apFunctoriality{}\) (called \verb+ap-seq-∙-=+ in the mechanization) doesn't take many lines of code:
\begin{code}
  ap-seq-∙-= : {a a' : A} → (s : a =-= a')
    → ap f (↯ s) == ↯ (ap-seq s)
  ap-seq-∙-= [] = idp
  ap-seq-∙-= (p ◃∙ []) = idp
  ap-seq-∙-= (idp ◃∙ s@(_ ◃∙ _)) = ap-seq-∙-= s

  ap-seq-∙ : {a a' : A} (s : a =-= a')
    → (ap f (↯ s) ◃∎) =ₛ ap-seq s
  ap-seq-∙ s = =ₛ-in (ap-seq-∙-= s)
\end{code}

Similarly, we define data types for compositions of maps, and for compositions of pointed maps and a DSL which helps formalize commutative diagrams of these maps.

My second piece of advice is: Get a fast computer with lots of memory. Checking our mechanized proof that the cup product is commutative takes more than 10\,GB of RAM on my machine. At the beginning of the mechanization effort I was working on a laptop with only 4\,GB of RAM. Needless to say, I was running out of memory constantly, which caused my system to slow down due to swapping. After I upgraded to a computer with 16\,GB of memory, this was no longer an issue.

One interesting development in the field of Homotopy Type Theory are cubical type theories. The design of these theories is motivated by the goal of giving computational meaning to the univalence axiom. One fundamental metatheorem about Martin-Löf type theory states that every closed term (a term that does not contain free variables) reduces to a value, that is a term of the form \(\Nsucc(\Nsucc(\ldots(\Nsucc(0))))\) with zero or more applications of the successor function. Adding the univalence breaks this metatheorem, since there are terms involving the univalence axiom for which no reduction rule applies. Cubical type theory solves this issue by defining univalence using a ``glueing operation'' which has good computational properties. One interesting property of cubical type theory is that some equalities, which only hold propositionally in HoTT, become judgemental equalities. Among them are \(\apCirc{}\) (which appears 50 times in this thesis), \cite[][Lemma 2.2.2 (iv)]{HoTTBook} (which appears 10 times) and \(\apCst{}\) (which appears 47 times)\footnote{There is a subtle issue here: In cubical type theories, there are identity types, which behave like the identity types from Martin-Löf Type Theory, and path types. These types are closely related, but not identical. The statement only holds for \(\apCirc{}\), Lemma 2.2.2~(iv) and \(\apCst{}\) suitably redefined using path types. For details see~\cite[][Section 7.2]{ModelOfTTInCubicalSets}.}.
Similarly, cubical type theories allow the beta rules for path constructors in higher inductive types to hold judgementally instead of only propositionally~\cite{OnHITsInCubicalTypeTheory}. If we had these judgemental equalities, the construction of several paths would become easier, but even more importantly, the diagrams involving these paths would become smaller too. It would be interesting to redo (part of) the mechanization in a proof assistant based on cubical type theory to see how much simpler it becomes.

\todo{remark about an integrated tool}